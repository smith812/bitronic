<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CHTTP::SetStatus("404 Not Found");
define("ERROR_404","Y");

$APPLICATION->SetPageProperty("title", "Страница не найдена");
// $APPLICATION->AddChainItem('404');

?>
<main class="container not-found-page">
	<div class="row">
		<div class="col-xs-12">
			<h1><?$APPLICATION->ShowTitle()?></h1>
			<p>Так сложились звезды, что этой страницы либо не существует, либо ее похитили инопланетяне для опытов. Но это не беда! Мы уверены, что вы обязательно найдете что-нибудь полезное для себя в нашем интернет-магазине.</p>
			<div class="sad-robot"></div>
			<div class="big404">404</div>
			<p>Перейти к <a href="<?=SITE_DIR?>site-map/" class="link"><span class="text">карте сайта</span></a>.</p>
		</div><!-- /.col-xs-12 -->
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>