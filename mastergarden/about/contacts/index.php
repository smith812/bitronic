<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<main class="container about-page">
	<div class="row">
		<div class="col-xs-12">
<h1><?$APPLICATION->ShowTitle()?></h1>


	<p>
		<b>Минск</b> 
			<div><b>г. Минск, ул. Физкультурная, 3
</b></div>
			<div><b>E-mail: sale@bitronic.test1.newsite.by</b></div>
			<div><b>тел. 8 (017) 297-33-66</b></div>
	</p>
	
	<p>
	<h3>Банковские реквизиты</h2>
	
		<div><b>ИНН: 190252909</b></div>
		<div><b>КПП: </b></div>
		<div><b>Расчетный счет: 0000 0000 0000 0000 0000</b></div>
		<div><b>Банк: ОАО "Банк"</b></div>
		<div><b>Банковские реквизиты: БИК 12345678</b></div>
		<div><b>Корреспондентский счет: </b></div>
	</p>
	
	<p>В нашем магазине всегда только самые лучшие товары по низким ценам. Каталог продаваемых товаров постоянно пополняется и обновляется.</p>

	<p>
		<?$APPLICATION->IncludeComponent(
			"bitrix:map.google.view",
			"",
			Array(
			"OPTIONS" => array(
				// 0 => "ENABLE_SCROLL_ZOOM",
				1 => "ENABLE_DBLCLICK_ZOOM",
				// 2 => "ENABLE_DRAGGING",
			),
			"MAP_WIDTH" => 'AUTO',
			)
		);?>
	</p>
	<p>
		<?$APPLICATION->IncludeComponent(
			"bitrix:map.yandex.view",
			"",
			Array(
			"OPTIONS" => array(
				// 0 => "ENABLE_SCROLL_ZOOM",
				1 => "ENABLE_DBLCLICK_ZOOM",
				// 2 => "ENABLE_DRAGGING",
			),
			"MAP_WIDTH" => 'AUTO',
			),
			false
		);?> 	 
	</p>
	<?if (CModule::IncludeModule('simai.maps2gis')): ?>
		<p>
			<?$APPLICATION->IncludeComponent(
				"simai:maps.2gis.simple",
				"",
				Array(
				"MAP_WIDTH" => 'AUTO',
				)
			);?>
		</p> 
	<?endif?>
		</div>
	</div>
</main>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>