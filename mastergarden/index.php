<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Bitronic2 - магазин бытовой и цифровой техники");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Главная страница");
global $rz_b2_options;
?>
<main class="home-page" data-page="home-page">
	<h1 class="home-page-h1"><?$APPLICATION->ShowTitle()?></h1>
	<?
	if($rz_b2_options['block_home-main-slider'] == 'Y' || $rz_b2_options["menu-catalog"] == "side") {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/big-slider.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	if($rz_b2_options['block_home-rubric'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/categories.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));
	}
	if($rz_b2_options['block_home-cool-slider'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/cool-slider.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	if($rz_b2_options['block_home-specials'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/main_spec.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	?>
	<?if ($rz_b2_options['block_home-our-adv'] == 'Y'):?>
		<div class="container hidden-xs">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/benefits.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"N"));?>
		</div>
	<? endif ?>
	<? if ('Y' == $rz_b2_options['block_home-feedback']): ?>
		<? \Yenisite\Core\Tools::IncludeArea('index', 'feedback', false, true) ?>
	<? endif ?>
	<? if ($rz_b2_options['block_home-catchbuy'] == 'Y') {
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/catchbuy.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));
	}
	?>
	
	<div class="promo-banners container wow fadeIn">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/banner1.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
		<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/banner2.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
	</div>
	
	<div class="text-content container wow fadeIn">
		<div class="row">
			<div class="col-sm-6 col-md-5">
				<div class="about">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/about_title.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"N"));?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/about_text.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"N"));?>
				</div>
			</div><!-- /.col-sm-6.col-md-5 -->

			<?if ($rz_b2_options['block_home-news'] == 'Y'):?>
				<div class="col-sm-6 col-md-5">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include_areas/index/news.php", "EDIT_TEMPLATE" => "include_areas_template.php"), false, array("HIDE_ICONS" => "Y"));?>
				</div><!-- /.col-sm-6.col-md-5 -->
			<?endif?>
			<?if($rz_b2_options['block_home-voting'] == 'Y'):?>
				<div class="col-md-2 hidden-sm hidden-xs">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/voting.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
				</div><!-- /.col-sm-2 -->
			<? endif ?>
		</div><!-- /.row -->
		<?if ($rz_b2_options['block_home-brands'] == 'Y'):?>
		<div class="row brands-wrap wow fadeIn" data-brands-view-type="<?= ($rz_b2_options['brands_cloud'] == 'Y') ? 'tags' : 'carousel' ?>">
			<div class="col-sm-12">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/index/brands.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
			</div><!-- /.col-sm-12 -->
		</div><!-- /.row.brands-wrap -->
		<? endif ?>
	</div><!-- /.text-content.container -->
</main><!-- /.home-page -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>