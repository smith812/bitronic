<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?>
<?if(CModule::IncludeModule('yenisite.bitronic2lite')):
	if(CModule::IncludeModule('yenisite.market')):?>
<?$APPLICATION->IncludeComponent("yenisite:catalog.basket", "bitronic2", Array(
	"PROPERTY_CODE" => array(
			0 => "FIO",
			1 => "EMAIL",
			2 => "PHONE",
			3 => "ABOUT",
			4 => "DELIVERY_E",
			5 => "PAYMENT_E",
		),
		"EVENT" => "SALE_ORDER",
		"EVENT_ADMIN" => "SALE_ORDER_ADMIN",
		"YENISITE_BS_FLY" => "",
		"EMPTY_URL" => "/mastergarden/personal/order/empty.php",
		"THANK_URL" => "/mastergarden/personal/order/thank_you.php",
		"ORDER_URL" => "/mastergarden/personal/orders/?ID=#ID#",
		"UE" => "Р",
		"ADMIN_MAIL" => "admin@email.ru",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"INCLUDE_JQUERY" => "Y",
		"RESIZER_BASKET_PHOTO" => "13",
	),
	false
);?><?
	else:?>
<main class="container">
<p style="margin-top:30px; color:red">
Ошибка. Модуль "Киоск" не установлен.
</p>
</main><?
	endif?>
<?else:?>
<?$APPLICATION->IncludeComponent("bitrix:sale.order.ajax", "bitronic2", array(
	"PAY_FROM_ACCOUNT" => "Y",
	"COUNT_DELIVERY_TAX" => "N",
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
	"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
	"ALLOW_AUTO_REGISTER" => "Y",
	"SEND_NEW_USER_NOTIFY" => "Y",
	"DELIVERY_NO_AJAX" => "N",
	"TEMPLATE_LOCATION" => "",
	"PROP_1" => array(
	),
	"PATH_TO_BASKET" => "/mastergarden/personal/cart/",
	"PATH_TO_PERSONAL" => "/mastergarden/personal/orders/",
	"PATH_TO_PAYMENT" => "/mastergarden/personal/payment/",
	"PATH_TO_ORDER" => "/mastergarden/personal/order/",
	"URL_SHOP_RULES" => '/mastergarden/about/',
	"RESIZER_BASKET_PHOTO" => "13",
	"SET_TITLE" => "Y" ,
	"DELIVERY2PAY_SYSTEM" => Array(),
	"SHOW_ACCOUNT_NUMBER" => "Y"
	),
	false
);?>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>