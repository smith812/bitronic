<?
$aMenuLinks = Array(
	Array(
		"Профили покупателя", 
		"/mastergarden/personal/profiles/",
		Array(),
		Array('ICON_CLASS'=> 'icon-user flaticon-user12'), 
		"CRZBitronic2Settings::isPro()" 
	),
	Array(
		"Настройки пользователя", 
		"/mastergarden/personal/profile/",
		Array(),
		Array('ICON_CLASS'=> 'icon-settings flaticon-settings1'), 
		"" 
	),
	Array(
		"История заказов", 
		"/mastergarden/personal/orders/",
		Array(),
		Array('ICON_CLASS'=> 'icon-history flaticon-open12'), 
		"" 
	),
	Array(
		"Настройки рассылки",
		"/mastergarden/personal/subscribe/",
		Array(),
		Array('ICON_CLASS'=> 'icon-subscribe flaticon-envelope29'),
		"IsModuleInstalled('subscribe')"
	),
	Array(
		"Выход",
		"?logout=yes",
		Array(),
		Array('ICON_CLASS'=> 'flaticon-logout13'),
		""
	),
);
?>