<?
$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	"modal", 
	array(
		"REGISTER_URL" => "/mastergarden/personal/?register=yes",
		"FORGOT_PASSWORD_URL" => "/mastergarden/personal/profile/",
		"PROFILE_URL" => "/mastergarden/personal/profile/",
		"SHOW_ERRORS" => "Y",
		"RESIZER_USER_AVA_ICON" => "8"
	),
	false
);