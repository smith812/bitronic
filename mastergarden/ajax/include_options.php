<?
global $rz_b2_options;

//fill rz_b2_options
$APPLICATION->IncludeComponent("yenisite:settings.panel", "empty", array(
		"SOLUTION" => $moduleId,
		"SETTINGS_CLASS" => $settingsClass,
		"GLOBAL_VAR" => "rz_b2_options",
		"EDIT_SETTINGS" => array()
	),
	false
);

ob_start();
//fill rz_b2_options['active-currency']
include $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include_areas/header/switch_currency.php';
ob_end_clean();

$fileSwitch = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include_areas/demoswitch.php';
if (file_exists($fileSwitch)) {
	include($fileSwitch);
}
?>
