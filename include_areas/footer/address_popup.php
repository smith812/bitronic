<?
global $rz_b2_options;
$pf = '';
if ('Y' == $rz_b2_options['change_contacts']) {
	$pf = $rz_b2_options['GEOIP']['INCLUDE_POSTFIX'];
	if(!empty($pf)) {
		$pf = '_' . $pf;
	}
}
if (strlen($address = $APPLICATION->GetFileContent($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include_areas/footer/address' . $pf . '.php')) > 0): ?>
	<? $APPLICATION->IncludeComponent(
		"bitrix:map.yandex.view",
		"popup",
		array(
			"ADDRESS_STR" => $address,
			"COMPONENT_TEMPLATE" => ".default",
			"INIT_MAP_TYPE" => "MAP",
			"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:53.86765654723551;s:10:\"yandex_lon\";d:27.558313758028987;s:12:\"yandex_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:27.558770766155;s:3:\"LAT\";d:53.867554929927;s:4:\"TEXT\";s:68:\"Мастер Гарден###RN###ул. Физкультурная, 3\";}}}",
			"MAP_WIDTH" => "600",
			"MAP_HEIGHT" => "500",
			"CONTROLS" => array(
				0 => "ZOOM",
				1 => "MINIMAP",
				2 => "TYPECONTROL",
				3 => "SCALELINE",
			),
			"OPTIONS" => array(
				0 => "ENABLE_SCROLL_ZOOM",
				1 => "ENABLE_DBLCLICK_ZOOM",
				2 => "ENABLE_DRAGGING",
			),
			"MAP_ID" => "address_modal_map"
		),
		false
	); ?>
<? endif ?>