<div class="row benefits hidden-xs hidden-sm wow fadeIn">
	<div class="benefit col-md-4">
		<div class="img-wrap">
			<span data-picture data-alt="Доставляем!">
			<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/delivery.png"></span>
			<span data-src="" data-media="(max-width: 767px)"></span>

			<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
				<noscript>
				 	<img src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/delivery.png" alt="Доставляем!">
				</noscript>
			</span>
		</div>
		<div class="content">
			<header>Быстро и качественно доставляем</header>
			<p>Наша компания производит доставку по всей России и ближнему зарубежью</p>
		</div>
	</div><div class="benefit col-md-4">
		<div class="img-wrap">
			<span data-picture data-alt="Гарантируем!">
				<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/OK.png"></span>
				<span data-src="" data-media="(max-width: 767px)"></span>

				<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
				<noscript>
					<img src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/OK.png" alt="Гарантируем!">
				</noscript>
			</span>
		</div>
		<div class="content">
			<header>Гарантия качества и сервисное обслуживание</header>
			<p>Мы предлагаем только те товары, в качестве которых мы уверены</p>
		</div>
	</div><div class="benefit col-md-4">
		<div class="img-wrap">
			<span data-picture data-alt="Обмениваем!">
			<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/exchange.png"></span>
			<span data-src="" data-media="(max-width: 767px)"></span>

			<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
			<noscript>
				<img src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/exchange.png" alt="Обмениваем!">
			</noscript>
			</span>		
		</div>
		<div class="content">
			<header>Возврат товара в течение 30 дней</header>
			<p>У вас есть 30 дней, для того чтобы протестировать вашу покупку</p>
		</div>
	</div>
</div>