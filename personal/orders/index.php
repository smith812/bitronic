<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("История заказов");

if(!$USER->IsAuthorized())
{
	$APPLICATION->AuthForm("");
	return;
}
?>
<main class="container">
	<h1><?$APPLICATION->ShowTitle()?></h1>
	<div class="account row">
		<?include '../left_menu.php';?>
		<div class="account-content col-xs-12 col-sm-9 col-xl-10">
		<?if (!CModule::IncludeModule('yenisite.bitronic2lite')):?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:sale.personal.order", 
				"bitronic2", 
				array(
					"PROP_1" => "",
					"PROP_2" => "",
					"SEF_MODE" => "N",
					"SEF_FOLDER" => "/personal/",
					"ORDERS_PER_PAGE" => "20",
					"PATH_TO_PAYMENT" => "/personal/payment/",
					"PATH_TO_BASKET" => "/personal/cart/",
					"SET_TITLE" => "N",
					"SAVE_IN_SESSION" => "Y",
					"NAV_TEMPLATE" => "",
					"PROP_5" => array(
					),
					"PROP_6" => array(
					),
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_GROUPS" => "Y",
					"CUSTOM_SELECT_PROPS" => array(
					),
					"HISTORIC_STATUSES" => array(
						0 => "F",
					),
					"RESIZER_BASKET_ICON" => "13"
				),
				false
			);?>
		<?else:?>
			<?
			$_REQUEST['ID'] = intval($_REQUEST['ID']);
			if ($_REQUEST['ID'] > 0):
			?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.detail", 
				"order", 
				array(
					"IBLOCK_TYPE" => "yenisite_market",
					"IBLOCK_ID" => "#MARKET_IBLOCK_ID#",
					"ELEMENT_ID" => $_REQUEST["ID"],
					"ELEMENT_CODE" => "",
					"CHECK_DATES" => "Y",
					"FIELD_CODE" => array(
						0 => "CREATED_BY",
						1 => "DATE_CREATE",
					),
					"PROPERTY_CODE" => array(
						2 => "FIO",
						4 => "PHONE",
						3 => "EMAIL",
						5 => "ABOUT",
						7 => "PAYMENT_E",
						8 => "DELIVERY_E",
					),
					"IBLOCK_URL" => SITE_DIR."personal/orders/",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "N",
					"CACHE_TIME" => "36000000",
					"CACHE_GROUPS" => "Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "Y",
					"BROWSER_TITLE" => "-",
					"SET_META_KEYWORDS" => "Y",
					"META_KEYWORDS" => "-",
					"SET_META_DESCRIPTION" => "Y",
					"META_DESCRIPTION" => "-",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"ADD_ELEMENT_CHAIN" => "N",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"USE_PERMISSIONS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Страница",
					"PAGER_SHOW_ALL" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"USE_SHARE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"RESIZER_BASKET_PHOTO" => "13"
				),
				false
			);?>
			<?else:?>
			<?
			global $USER;
			if($USER->GetID()):
				global $arrFilter;
				$arrFilter = array();
				$arrFilter['CREATED_BY'] = $USER->GetID() ;
				$arrFilter['PROPERTY_SITE_ID'] = SITE_ID;
				?>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "orders", Array(
						"IBLOCK_TYPE" => "yenisite_market",	// Тип информационного блока (используется только для проверки)
						"IBLOCK_ID" => "#MARKET_IBLOCK_ID#",	// Код информационного блока
						"NEWS_COUNT" => "100",	// Количество новостей на странице
						"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
						"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
						"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
						"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
						"FILTER_NAME" => "arrFilter",	// Фильтр
						"FIELD_CODE" => array(	// Поля
							0 => "ID",
							1 => "DATE_CREATE",
							2 => "",
						),
						"PROPERTY_CODE" => array(	// Свойства
							0 => "ITEMS",
							1 => "STATUS",
							2 => "AMOUNT",
							3 => "SITE_ID"
						),
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"DETAIL_URL" => "?ID=#ID#",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
						"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
						"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
						"HIDE_LINK_WHEN_NO_DETAIL" => "Y",	// Скрывать ссылку, если нет детального описания
						"PARENT_SECTION" => "",	// ID раздела
						"PARENT_SECTION_CODE" => "",	// Код раздела
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"PAGER_TITLE" => "Заказы", // Название категорий
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"DISPLAY_DATE" => "Y",	// Выводить дату элемента
						"DISPLAY_NAME" => "Y",	// Выводить название элемента
						"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
						"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
					),
					false,
					array("HIDE_ICONS" => "Y")
				);?>
				<?endif?>
			<?endif?>
		<?endif?>
		</div>
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>