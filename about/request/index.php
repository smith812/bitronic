<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявка на запчасти");
?>
<main class="container about-page">
	<div class="row" style="max-width: 700px; margin: auto;">
		<div class="col-xs-12" style="padding: 20px;">
			<h1><?$APPLICATION->ShowTitle()?></h1>

			<?if (CModule::IncludeModule("yenisite.feedback")):?>
				<?$APPLICATION->IncludeComponent(
	"yenisite:feedback.add", 
	"request", 
	array(
		"IBLOCK_TYPE" => "bitronic2_feedback",
		"IBLOCK" => "50",
		"NAME_FIELD" => "NAME",
		"COLOR_SCHEME" => "green",
		"TITLE" => "Заявка на запчасти",
		"SUCCESS_TEXT" => "Спасибо! Ваша заявка принята! После обработки наш специалист свяжется с Вами.",
		"USE_CAPTCHA" => "Y",
		"SHOW_SECTIONS" => "N",
		"PRINT_FIELDS" => array(
			0 => "LIST",
			1 => "TEXT",
			2 => "NAME",
			3 => "EMAIL",
			4 => "PHONE",
		),
		"AJAX_MODE" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"ACTIVE" => "Y",
		"EVENT_NAME" => "REQUEST",
		"TEXT_REQUIRED" => "N",
		"TEXT_SHOW" => "N",
		"NAME" => "NAME",
		"PHONE" => "PHONE",
		"FORM" => "form_feedback",
		"EMPTY" => $arParams["EMPTY"],
		"COMPONENT_TEMPLATE" => "request",
		"SECTION_CODE" => "",
		"ELEMENT_ID" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"EMAIL" => "EMAIL"
	),
	false,
	array(
		"HIDE_ICONS" => "N"
	)
);
				?>
			<? endif ?>
		</div>
	</div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>