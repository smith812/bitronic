<?
$MESS ['BITRONIC2_FEED_MODAL_CAPTCHA_TITLE'] = 'Введите код';
$MESS ['BITRONIC2_FEED_MODAL_CAPTCHA_ALT'] = 'Защита от автоматических сообщений';
$MESS ['BITRONIC2_FEED_MODAL_SECTION_SELECT'] = 'Выберите раздел';
$MESS ['BITRONIC2_FEED_MODAL_MESSAGE'] = 'Сообщение';
$MESS ['BITRONIC2_FEED_MODAL_SEND'] = 'Отправить';
$MESS ['BITRONIC2_FEED_MODAL_ERROR'] = 'Ошибка!';
$MESS ['BITRONIC2_FEED_MODAL_REQUIRED'] = 'Поля, отмеченные звездочкой, обязательны для заполнения';
$MESS ['BITRONIC2_FEED_MODAL_SUBTITLE'] = 'Если Вам требуется отсутствующий на складе/в каталоге товар или запчасть (с известным артикулом), можете отправить нам "заявку на Швецию". В этом случае вы не получите счет моментально, как при обычном оформлении заказа, зато переложите все заботы на нашего менеджера. Он уточнит условия поставки, согласует с Вами сроки и цены на позиции по вашему списку и пришлет счет на оплату.';