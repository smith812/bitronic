$(function(){
    $(document).on('click', '.table-add-row a', function(){
        if($('.list-wrap tr').length > 20) return;
        $('.list-wrap tbody').append($('#table-row tbody').html());
    });

    $(document).on('click', '.list-wrap tr:last td input.art', function(){
        if($(this).val() == ''){
            $('.table-add-row a').click();
        }
    });

    $(document).on('click', '#form-request button[type=submit]', function(){
        var list = "\n";
        $('.list-wrap tbody tr').each(function(){
            var art = $(this).find('.art').val().replace(/\s/g, '');
            if(art.length){
                var arr = 'Арт: ' + $(this).find('.art').val() + ',';
                arr = arr + 'Название: ' + $(this).find('.name').val() + ',';
                arr = arr + 'Кол-во: ' + $(this).find('.count').val() + '; ';
                list = list + arr + "\n";
            }
        })
        $('#field-LIST').val(list);
        return true;
    })
})