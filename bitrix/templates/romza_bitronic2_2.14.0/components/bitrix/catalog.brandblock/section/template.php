<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
// echo "<pre style='text-align:left;'>";print_r($arResult);echo "</pre>";

$id = 'bx_dynamic_'.$this->randString(20);
?>

<div class="brands-catalog hidden-xs" id="<?=$id?>"><?

	$frame = $this->createFrame($id, false)->begin();
	if (!empty($arResult['ITEMS'])):
		foreach ($arResult['ITEMS'] as $arItem):
	
				?><div class="brand">
					<a href="<?=$arItem['LINK']?>" class="brand-img">
						<img src="<?=$arItem['PICT']['SRC']?>" alt="<?=$arItem['NAME']?>">
					</a>
					<?//TODO <sup>43</sup>?>
				</div><?

		endforeach;
	endif;
	include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info.php';
	$frame->end();
	?>
			</div><!-- /.brands -->

<? unset($_SESSION['RZ_SECTION_BRANDS']) ?>
