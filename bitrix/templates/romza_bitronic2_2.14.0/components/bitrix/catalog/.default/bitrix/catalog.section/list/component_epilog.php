<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

//Needs for iblock.vote to not break composite
IncludeAJAX();

global $arPagination;
$arPagination = $arResult['NAV_PAGINATION'];

$frame = new \Bitrix\Main\Page\FrameBuffered("section_{$templateName}_epilog_JCCatalogItem");
$frame->setAssetMode(\Bitrix\Main\Page\AssetMode::STANDARD);
$frame->begin('');
if (!empty($templateData['jsFile'])) {
	if (\Yenisite\Core\Tools::isAjax() || $_SERVER['HTTP_BX_AJAX'] !== null) {
		$jsString = file_get_contents(rtrim($_SERVER['DOCUMENT_ROOT'], '/\\') . $templateData['jsFile']);
		echo '<script>', $jsString, '</script>';
	} else {
		\Bitrix\Main\Page\Asset::getInstance()->addJs($templateData['jsFile']);
	}
}
$frame->end();
