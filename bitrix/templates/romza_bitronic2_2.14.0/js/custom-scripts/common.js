var dealersMapCollection,
    dealersMap;

$(function(){
    $(document).on('click', '.my-cost, .quantity-change, button.buy', function(){
        requestProductCount($(this), $(this).hasClass('increase'));
    })
    $(document).on('change', '.quantity-input', function(){
        requestProductCount($(this), true);
    })

    $(document).on('click', '#modal_productcount button.click-buy', function(){
        var val = $(this).data('do');
        var id = '#' + $('#modal_productcount .current-input').data('id');
        if(val == 'down'){
            $(id).val($('#modal_productcount .current-input').data('count'));
        }else if(val = 'count'){
            var count = $('#modal_productcount input.quantity-input').val();
            $(id).val(count);
        }
        var source = $($('#modal_productcount .current-input').data('source'));
        if(source.hasClass('catalog-table-item')){
            $('#add_basket_table').click();
        }else {
            source.find('button.buy').click();
        }

        $('#modal_productcount .btn-close').click();
    })

    initQuantityChange('#modal_productcount');
    //$(document).on('click', '#modal_productcount .request-count')
    $(document).on('change', '.dealers-filter select, .dealers-filter input', function(){
        dealersFilterChange();
    })
    $(document).on('click', '.dealers-table-small .item .caption', function(){
        var id = 0;
        if($('.dealers-table-small .item.active').length){
            id = $('.dealers-table-small .item.active').data('id');
            $('.dealers-table-small .item.active').removeClass('active');
        }
        $('.dealers-table-small .item .description').slideUp('slow');
        if(id != $(this).closest('.item').data('id'))
        {
            $(this).closest('.item').find('.description').slideDown('slow');
            $(this).closest('.item').addClass('active');
        }
    });
    $(document).on('change', '#modal_productcount .quantity-input', function(){
        var min = parseInt($(this).data('min'));
        if (isNaN(min) || min < 1) min = 1;
        var step = parseInt($(this).data('step'));
        if (isNaN(step) || step < 1) step = 1;
        var val = parseInt($(this).val());
        if (isNaN(val) || val < min) {
            val = min;
        }else{
            val = Math.ceil(val / step) * step;
        }
        $(this).val(val);
    })
    catalogSectionQuickViewElement();
})

function getCatalogItemContainer(el){
    if(el.closest('.product-main').length){
        return el.closest('.product-main');
    }else if(el.closest('.catalog-item').length){
        return el.closest('.catalog-item');
    }else if(el.closest('.ajax-search-item').length){
        return el.closest('.ajax-search-item');
    }else if(el.closest('.table-item').length){
        return el.closest('.table-item');
    }else if(el.closest('.product-options').length){
        return el.closest('.product-options');
    }else return false;
}

function requestProductCount(el, viewPopup){

    var container = getCatalogItemContainer(el);
    if(container == false) return;
    var input = container.find('input.quantity-input');
    if(!input.length) return;
    if(input.closest('#modal_productcount').length) return;

    var id = parseInt(input.data('id'));
    if(!id) return;
    var requestCount = parseInt(input.val());

    var options = {
        url: '/local/components/newsite/request.product.count/ajax.php?id=' + id + '&count=' + requestCount,
        dataType: "json",
        type: "POST",
    };

    $.ajax(options).done(function(data)
    {
        if(data.has_error == '1'){
            return;
        }
        var count = parseInt(data.count);
        var price = parseInt(data.price);
        var countOrder = parseInt(data.count_order);
        var step = parseInt(data.step);
        var minOrder = parseInt(data.min_order);
        input.val(countOrder);
        requestCount = countOrder;

        catalogCalcMyCost(input, price, container);

        if(viewPopup && (count < requestCount)){
            var upcount = requestCount - count;
            $('#modal_productcount .count').text(count);
            $('#modal_productcount input.quantity-input').val(requestCount);
            $('#modal_productcount .current-input').attr('data-count', count);
            $('#modal_productcount .quantity-input').attr('data-step', step);
            $('#modal_productcount .quantity-input').attr('data-min', minOrder);
            $('#modal_productcount .request').text(requestCount);
            $('#modal_productcount .upcount').text(upcount);
            //input.addClass('requested');
            $('#modal_productcount .current-input').attr('data-id', input.attr('id'));
            $('#modal_productcount .current-input').attr('data-source', '#' + container.attr('id'));
            $('a.productcount-link').click();
        }
        input.css('style');
    });
}


function catalogCalcMyCost(el, priceValue, container){
    var priceClass = '.price';
    if(container.hasClass('ajax-search-item')){
        priceClass = '.item-price';
    }
    var price = container.find(priceClass);
    if(!price.length) return;

    var value = price.find('.value');

    container.find('.my-cost').attr('disabled', true);
    container.find('.my-cost').addClass('calc');
    container.find('.price').addClass('calc');
    var oldPriceValue = parseFloat(value.text().replace(/\s/g, ''));
    if(oldPriceValue != priceValue){
        value.html('<img src="' + SITE_TEMPLATE_PATH + '/img/ajax-loader.gif" />');
        setTimeout(function(){
            priceValue = priceValue.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
            value.html(priceValue);
            value.addClass('calc');
            container.find('.tooltip').hide();
        }, 700);
    }
}

function initQuantityChange(target){
    var timerQuantity;
    $(target).find('.quantity-counter').on({
        mousedown: function(){
            var _ = $(this);
            var quanInput = _.parent().find('.quantity-input');
            if ( quanInput.hasClass('disabled') || quanInput.is(':disabled') || _.hasClass('disabled') ) return;
            var curValue = parseInt(quanInput.val(), 10);

            var changeStep = parseInt(quanInput.attr('data-step'));
            if (isNaN(changeStep) || changeStep < 1) changeStep = 1;
            var min = parseInt(quanInput.attr('data-min'));
            if (isNaN(min) || min < 1) min = 1;
            if ( _.hasClass('decrease') ) changeStep = -1 * changeStep;
            if (isNaN(curValue) || curValue<1) curValue = 1;


            curValue += changeStep;
            if ( curValue < min ) curValue = min;
            quanInput.val(curValue);
            timerQuantity = setInterval(function(){
                curValue += changeStep;
                if ( curValue < min ) curValue = min;
                quanInput.val(curValue);
            }, 100);
        },
        'mouseup mouseleave': function(){
            clearInterval(timerQuantity);
        }
    }, '.quantity-change').on('change', '.quantity-input', function(){
        var _ = $(this);
        var newValue = _.val();
        if (isNaN(newValue) || newValue < 1) _.val(1);
    })
}

$(function(){
    if($('#dealers-map').length){
        ymaps.ready(function(){
            dealersMap = new ymaps.Map("dealers-map", {
                center: [53.90, 27.56],
                zoom: 11,
                controls: ['zoomControl', 'fullscreenControl']
            });
            dealersMap.behaviors.disable('scrollZoom');
            dealersMapCollection = new ymaps.GeoObjectCollection();

            dealersFilterChange();
        });
    }
})

function addBallunsToBranchesMap(){
    ymaps.ready(function() {
        var geocoder;
        var mainPlacemark;
        var bodyHTML;
        var placemarks = [];
        var long;
        var lat;
        var regionText = $(".filter select[name=region] option:selected").text();
        if(regionText.substr(-1) == 'я'){
            regionText = regionText + ' область';
        }

        dealersMapCollection.removeAll();

        $('.dealers-table-small .item').removeClass('active').find('.description').slideUp();
        $('.dealers-table-small .item').hide();
        $(".dealers-table .catalog-item").each(function(){
            if($(this).css('display') != 'none'){
                $('.dealers-table-small .item[data-id=' + $(this).data('id') + ']').show();

                bodyHTML = '<div class="dealers-map-info"><h3>' + $(this).find('.name').text() + '</h3>';

                bodyHTML = bodyHTML + $(this).find('.city').text() + ' ' + $(this).find('.address').text() + '<br>'  +
                    $(this).find('.contacts').text();

                bodyHTML = bodyHTML + '</div>';
                lat = $(this).data('lat');
                long = $(this).data('long');


                mainPlacemark = new ymaps.Placemark([lat, long], {
                    hintContent: $(this).find('td .name').text(),
                    balloonContentBody: bodyHTML
                }, {
                    /*iconLayout: 'default#image',
                    iconImageHref: '/local/images/map/map-balloon' + $(this).data("balloon") + '.png',
                    iconImageSize: [39, 54]*/
                });
                dealersMapCollection.add(mainPlacemark);
                placemarks.push(mainPlacemark);
            }
        });

        if(placemarks.length){
            var clusterer = new ymaps.Clusterer();
            clusterer.add(placemarks);
            dealersMapCollection.add(clusterer);
            dealersMap.geoObjects.add(dealersMapCollection);
            dealersMap.setBounds(dealersMapCollection.getBounds());
            var zoom = dealersMap.getZoom();
            if(!isNaN(zoom)){
                if (zoom > 15) zoom = 15;
                dealersMap.setZoom(zoom);
            }
        }
    });
}

function dealersFilterChange(){
    var region = $('.dealers-filter select[name=region]').val();
    var brand = $('.dealers-filter select[name=brand]').val();
    var showroom = $('.dealers-filter input[name=showroom]').is(':checked');
    var service_senter = $('.dealers-filter input[name=service_senter]').is(':checked');
    var online_store = $('.dealers-filter input[name=online_store]').is(':checked');

    $(".dealers-table .catalog-item").each(function(){
        var view = true;
        if(region != 'all'){
            view = view && (region == $(this).data('region'));
        }
        view = view && dealersCheckBrand(this, brand);
        if(showroom){
            view = view && ($(this).data('showroom') == '1');
        }
        if(service_senter){
            view = view && ($(this).data('service_senter') == '1');
        }
        if(online_store){
            view = view && ($(this).data('online_store') == '1');
        }
        if(view){
            $(this).show();
        }else{
            $(this).hide();
        }
    });
    addBallunsToBranchesMap();
}

function dealersCheckBrand(row, brand){
    if(brand == 'all') return true;
    var view = false;
    $(row).find('.brands li').each(function(){
        view = view || ($(this).data('id') == brand);
    })
    return view;
}

function catalogSectionQuickViewElement(){
    var search = window.location.search;
    search = search.match(/\?\S*viewFastInfo=1&itemId=(\d+)\S*/i);
    if(search === null) return;
    var id = search[1];

    /*setTimeout(function(){
        if(!$('.more-catalog-wrap a.more-catalog').hasClass('disabled')){
            $('.more-catalog-wrap a.more-catalog').click();
        }
    }, 1500);*/

    setTimeout(function(){
        var item = $('.catalog .quantity-input[data-id=' + id + ']');
        if(item.length){
            $("body,html").animate({"scrollTop":item.closest('.catalog-item').offset().top - 60},1500);
            item.closest('.catalog-item').find('.name span.link').click();
        }
    }, 3000);

}
