function initGenInfoToggle(){
	// check if function is needed at all
	var genInfo = $('.general-info');
	if (!genInfo || !genInfo.length) return;

//======================= VARIABLES ==========================
	var that = this,
		genDesc = genInfo.find('.desc'),
		genInfoToggle = genInfo.find('.link'),
		infoHeightLimit, genInfoScrollHeight, isToggleable;

//======================= METHODS ============================
	this.update = function(){
		if (!genDesc || !genDesc.length) return;
		// reset, just in case of something.
		genDesc.css('max-height', '');
		genInfo.removeClass('opened');

		infoHeightLimit = parseInt(genDesc.css('max-height'));
		genInfoScrollHeight = genDesc.get(0).scrollHeight;

		// button is added only if content is higher than limit by more than 20px
		// otherwise, open content fully
		if ( genInfoScrollHeight - infoHeightLimit > 20 ){
			genInfo.removeClass('opened');
			genInfoToggle.show();
			isToggleable = true;
		} else {
			genInfo.addClass('opened');
			genInfoToggle.hide();
			isToggleable = false;
		}

		if (b2.s.detailTextDefault === 'open' && isToggleable) that.open();
	};
	this.open = function(){
		genDesc.velocity({
			'max-height': genInfoScrollHeight
		}, 250, function(){
			genInfo.addClass('opened');
		})
	};
	this.close = function(){
		genDesc.velocity({
			'max-height': infoHeightLimit
		}, 250, function(){
			genInfo.removeClass('opened');
		});
	};

//======================= EVENTS =============================
	genInfoToggle.off('click.genInfoToggle').on('click.genInfoToggle', function(e){
		if (!genInfo || !genInfo.length) return;
		
		genInfo.hasClass('opened') ? that.close() : that.open();			
	});
	genDesc.find('img').off('load.genInfoToggle').on('load.genInfoToggle', function(){
		that.update();
	});
//======================= INIT ===============================
	this.update();
	b2.quickViewGenInfoInited = true;
}