<?if(!$USER->IsAuthorized()) return;
$objCountLevel = CCountLevels::getInstance();
if(isset($arItem)){
    $level = $objCountLevel->GetLevel($arItem['IBLOCK_SECTION_ID'], $arItem['CATALOG_QUANTITY']);
}else{
    $level = $objCountLevel->GetLevel($arResult['IBLOCK_SECTION_ID'], $arResult['CATALOG_QUANTITY']);
}
if(empty($level)) $level = 1;
?>
<span class="avail-dot count-levels count-level-<?=$level?>" data-text="Под заказ"></span>
