<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
  * @var $availableClass (string) - add text inside class attribute
  * @var $availableFrame (bool) - do we need to create dynamic frame
  * @var $availableID (string) - add attribute "id" with variable's content
  * @var $availableItemID (int) - iblock element id
  * @var $availableMeasure (string) - CATALOG_MEASURE_NAME
  * @var $availableOnRequest (bool) - is our product available on request only
  * @var $availableQuantity (float) - CATALOG_QUANTITY
  * @var $availableStoresPostfix (string) - postfix to add to stores container id attribute
  * @var $availableSubscribe (string) - Y if can subscribe
  * @var $bShowEveryStatus (bool) - do we need to put every status into html (for SKU switch)
  * @var $bShowStore (bool) - do we need to show stores popup
  **/

global $rz_b2_options;
global $rz_b2_storeCount;

if ($headerLangIncluded !== true) {
	\Bitrix\Main\Localization\Loc::loadMessages(SITE_TEMPLATE_PATH . '/header.php');
	$headerLangIncluded = true;
}

if ($bShowStore) {
	if (!isset($rz_b2_storeCount)) {
		CModule::IncludeModule('catalog');

		$filter = array(
			"ACTIVE" => "Y",
			"+SITE_ID" => SITE_ID,
			"ISSUING_CENTER" => 'Y'
		);

		$rz_b2_storeCount = CCatalogStore::GetList(
			array('TITLE' => 'ASC', 'ID' => 'ASC'),
			$filter,
			array() // to fetch only count of stores
		);
	}
	$bShowStore = ($rz_b2_storeCount > 0);
}

$availableFrameID = 'bxdinamic_availability_' . $this->randString();
?>

<div class="availability-info"<?if($availableFrame === true):?> id="<?=$availableFrameID?>"<?endif?>>
<?
if ($availableFrame === true) {
	$frame = $this->createFrame($availableFrameID, false)->begin(CRZBitronic2Composite::insertCompositLoader());
}
?>
<div class="availability-status <?=empty($availableClass)?'':$availableClass?>"<?if(!empty($availableID)):?> id="<?=$availableID?>"<?endif?>>
<? if(!empty($bShowEveryStatus) || $availableClass == 'in-stock' || empty($availableClass)): ?>
	<div class="when-in-stock">
		<?include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/count-level-dots.php';?>
		<?if(empty($noShowMyCost)):?>
			<button type="button" class="btn-action my-cost" data-compare-id="653"
					data-tooltip=""
					title="" id="" data-original-title="Запросить из 1С мою<br>персональную цену<br>для данного количества">Моя цена</button>
		<?endif?>
	</div><!-- /.when-in-stock -->
<? endif ?>
<? if(!empty($bShowEveryStatus) || $availableClass == 'available-for-order' || $availableClass == 'available-on-request'): ?>
	<div class="when-available-for-order<?if($availableOnRequest):?> when-available-on-request<?endif?>">
		<div class="info-tag" <? /* TODO 
			title="title" 
			data-tooltip
			data-placement="bottom"
			data-toggle="modal"
			data-target="#modal_place-order"*/?>
			>
			<?/*?><span class="text on-request"><?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_ON_REQUEST')?>111</span>
			<span class="text  for-order"><?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_FOR_ORDER') ?>222</span><?*/?>
			<?/*?><span class="text on-request">
				<span class="available-for-order">
					<span class="avail-dot when-available-for-order" data-text="Под заказ"></span>
				</span>
			</span>
			<span class="text  for-order">
				<span class="available-for-order">
					<span class="avail-dot when-available-for-order" data-text="Под заказ"></span>
				</span>
			</span><?*/?>
			<?include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/count-level-dots.php';?>
			<?if(empty($noShowMyCost)):?>
				<button type="button" class="btn-action my-cost" data-compare-id="653"
						data-tooltip=""
						title="" id="" data-original-title="Запросить из 1С мою<br>персональную цену<br>для данного количества">Моя цена</button>
			<?endif?>
		</div>
		<?/*?><div class="info-info">
			<?$APPLICATION->IncludeComponent('bitrix:main.include', '', array("PATH" => SITE_DIR."include_areas/catalog/for_order_text.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => "include_areas_template.php"), $component, array("HIDE_ICONS"=>"Y"))?>
		</div><?*/?>
	</div>
<? endif ?>
<? if(!empty($bShowEveryStatus) || $availableClass == 'out-of-stock'): ?>
	<div class="when-out-of-stock">
		<div class="info-tag"
			<? if ($availableSubscribe == 'Y' && $availableClass == 'out-of-stock'): ?>
			title="<?=GetMessage('BITRONIC2_PRODUCT_SUBSCRIBE')?>"
			data-tooltip
			data-product="<?= $availableItemID ?>"
			data-placement="bottom"
			data-toggle="modal"
			data-target="#modal_subscribe_product"
			<? endif ?>
			>
			<span class="text"><?=GetMessage('BITRONIC2_PRODUCT_AVAILABLE_FALSE')?></span>
		</div><!-- .info-tag -->
	</div>
<? endif ?>
</div><!-- .availability-status -->
<? if ($availableFrame === true) $frame->end() ?>
</div><!-- .availability-info -->
