<?

require 'include/events.php';
require 'include/countlevels.php';

define('DIALERS_IBLOCK_ID', 51);
define('CATALOG_IBLOCK_ID', 48);

function prent($mas, $prent = true, $show = false) {

    global $USER;

    if ($USER->IsAdmin() || $show) {
        echo "<pre style=\"text-align:left; background-color:#CCC;color:#000; font-size:10px; padding-bottom: 10px; border-bottom:1px solid #000;\">\n";
        if ($prent)
            print_r($mas);
        else
            var_dump($mas);
        echo "</pre>\n";
    }
}
