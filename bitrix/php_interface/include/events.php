<?

AddEventHandler('sale', 'OnBeforeBasketAdd', Array('CBasketEvents', 'OnBeforeBasketAdd'));
AddEventHandler('sale', 'OnBeforeBasketUpdate', Array('CBasketEvents', 'OnBeforeBasketUpdate'));

class CBasketEvents {


    public static function GetCurrentBasketPrice($id, $count)
    {
        $arPrice = self::GetCurrentBasketPriceRange($id);
        foreach($arPrice as $cnt => $val){
            if($count >= $cnt) break;
        }
        return $val;
    }

    /**
     * метод выдаёт массив соответствия КОЛ-ВО => ЦЕНА, отсортированный по ключу в обратном порядке
     * @param $id
     * @return mixed
     */
    public static function GetCurrentBasketPriceRange($id){

        if(!is_set($_SESSION['REQUEST_PRODUCT_COUNT'][$id]['PRICE']) ||
            !is_array($_SESSION['REQUEST_PRODUCT_COUNT'][$id]['PRICE'])){

            $priceData = CatalogGetPriceTableEx($id, 0, 1);
            $price = floatval($priceData['MATRIX'][1][0]['PRICE']);
            $minPrice = $price / 2;
            if(!empty($price)){
                $i = 0;
                $price = round(floatval($price) * 0.99 * 1000) / 1000;
                $arPrice[1] = $price;
                while(($i < 5) && ($price > $minPrice)){
                    $i++;
                    $price = $price - 20000 * $i;
                    $arPrice[($i * 5)] = $price;
                }
            }
            krsort($arPrice);

        }else {
            $arPrice = $_SESSION['REQUEST_PRODUCT_COUNT'][$id]['PRICE'];
        }
        return $arPrice;
    }

    public static function GetAvaibleProductCount($id){
        if(!is_set($_SESSION['REQUEST_PRODUCT_COUNT'][$id]['COUNT'])){
            $_SESSION['REQUEST_PRODUCT_COUNT'][$id]['COUNT'] = rand(3, 10);
        }
        return $_SESSION['REQUEST_PRODUCT_COUNT'][$id]['COUNT'];
    }

    function OnBeforeBasketAdd(&$arFields) {
        $arFields['QUANTITY'] = self::GetProductCountBySettings($arFields['PRODUCT_ID'], $arFields['QUANTITY']);
        $arFields['PRICE'] = self::GetCurrentBasketPrice($arFields['PRODUCT_ID'], $arFields['QUANTITY']);
    }

    public static function GetProductCountBySettings($id, $count){
        if(empty($count) && ($count > 0)) return 0;

        $minOrder = self::GetProductMinOrder($id);
        $stepOrder = self::GetProductStepOrder($id);
        return $count < $minOrder ? $minOrder : ceil($count / $stepOrder) * $stepOrder;
    }

    function OnBeforeBasketUpdate($ID, &$arFields) {
        $basketItem = CSaleBasket::GetByID($ID);
        $arFields['QUANTITY'] = self::GetProductCountBySettings($basketItem['PRODUCT_ID'], $arFields['QUANTITY']);
        $arFields['PRICE'] = self::GetCurrentBasketPrice($basketItem['PRODUCT_ID'], $arFields['QUANTITY']);
    }

    public static function GetProductMinOrder($id){
        if(empty($_SESSION['REQUEST_PRODUCT_COUNT'][$id]['MIN_ORDER']))
        {
            \Bitrix\Main\Loader::includeModule("iblock");
            $arr = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $id, array(), array('ID' => 1323))->Fetch();
            $_SESSION['REQUEST_PRODUCT_COUNT'][$id]['MIN_ORDER'] = empty($arr['VALUE']) ? 1 : $arr['VALUE'];
        }
        return $_SESSION['REQUEST_PRODUCT_COUNT'][$id]['MIN_ORDER'];
    }

    public static function GetProductStepOrder($id){
        if(empty($_SESSION['REQUEST_PRODUCT_COUNT'][$id]['STEP_ORDER']))
        {
            \Bitrix\Main\Loader::includeModule("iblock");
            $arr = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $id, array(), array('ID' => 1324))->Fetch();
            $_SESSION['REQUEST_PRODUCT_COUNT'][$id]['STEP_ORDER'] = empty($arr['VALUE']) ? 1 : $arr['VALUE'];
        }
        return $_SESSION['REQUEST_PRODUCT_COUNT'][$id]['STEP_ORDER'];
    }



}
