<?

class CCountLevels{

    protected static $_instance;
    protected  $sectionsLevels = array();

    private function __construct() {
        \Bitrix\Main\Loader::includeModule("iblock");
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __clone() {
    }

    private function __wakeup() {
    }

    private function GetSectionsChain($sectId)
    {
        $dbl = CIBlockSection::GetNavChain(CATALOG_IBLOCK_ID, $sectId, array('ID'));
        $res = array();
        while($arr = $dbl->Fetch()){
            $res[] = $arr['ID'];
        }
        return $res;
    }

    private function GetSectionCountLevels($sectId)
    {
        global $USER_FIELD_MANAGER;

        $sections = $this->GetSectionsChain($sectId);
        if(empty($sections)) return array();
        $setSections = array();

        foreach($sections as $sect)
        {
            if(empty($this->sectionsLevels[$sect]))
            {
                $res = array();
                $setSections[] = $sect;
                $arr = $USER_FIELD_MANAGER->GetUserFields('IBLOCK_48_SECTION', $sect);
                if(!empty($arr['UF_LEVELS']))
                {
                    $res = $arr['UF_LEVELS']['VALUE'];
                }
                sort($res);
                arsort($res);
            }else{
                $res = $this->sectionsLevels[$sect];
            }
            if(!empty($res)){
                foreach($setSections as $sId){
                    $this->sectionsLevels[$sId] = $res;
                }
                return $res;
            }
        }
        return array();
    }

    public function GetLevel($sect, $count){
        $levels = $this->GetSectionCountLevels($sect);
        foreach($levels as $k => $v){
            if($count >= $v) return ($k + 1);
        }
    }

}