<?
$MESS ['DISPLAY_PROPERTIES'] = 'Массив параметров, которые следует отсортировать';
$MESS ['YS_BS_COLOR'] = 'Цветовая схема';
$MESS ['YS_BS_COLOR_RED'] = 'Огонь';
$MESS ['YS_BS_COLOR_BLUE'] = 'Лед';
$MESS ['YS_BS_COLOR_GREEN'] = 'Жизнь';
$MESS ['YS_BS_COLOR_YELLOW'] = 'Золото';
$MESS ['YS_BS_COLOR_METAL'] = 'Пыль';
$MESS ['YS_BS_COLOR_PINK'] = 'Бэбибум';
$MESS ['SHOW_PROPERTY_VALUE_DESCRIPTION'] = 'Выводить описание значений свойств';
$MESS ['IBLOCK_ID_PARAM'] = 'Идентификатор инфоблока';
?>
