<?
/**
 * @author Ilya Faleyev <isfaleev@gmail.com>
 * @copyright 2004—2014 (c) ROMZA
 */
$MESS ['YS_YM_VENDOR_NAME'] = "Настройки vendor.model";
$MESS ['DEVELOPER'] = "Производитель (brand)";
$MESS ['FEED_NAME'] = 'Название фида с данными (title)';
$MESS ['FEED_DESCRIPTION'] = 'Описание фида с данными (description)';
$MESS ['MARKET_CATEGORY_CHECK'] = "Выводить google_product_category?";
$MESS ['MARKET_CATEGORY_PROP'] = "Свойство где хранится google_product_category";
$MESS ['GOOGLE_GTIN'] = "Код международной маркировки и учета логистических единиц(GTIN)";
$MESS ['FORCE_CHARSET'] = "Принудительно выставлять кодировку";

$MESS ['MARKET_CATEGORY_CHECK_TIP'] = 'Рекомендуется указывать для товара категорию в соответствии с документом <a href="http://www.google.com/basepages/producttype/taxonomy.ru-RU.txt">Google product taxonomy</a>.<br/><br/>Если не указано или у товара не заполнено свойство с категорией, в качестве категории будет выведена цепочка разделов инфоблока.';
/*
$MESS ['COUNTRY'] = "Страна производитель";
$MESS ['PARAMS'] = "Параметры";
$MESS ["COND_PARAMS"] = "Свойства, значения которых должны быть доступны в шаблоне для создания условий";
*/
?>