<?
$MESS ['IBLOCK_MODULE_NOT_INSTALLED'] = "Модуль Информационных блоков не установлен";
$MESS ['CATALOG_SECTION_NOT_FOUND'] = "Раздел не найден.";
$MESS ['CATALOG_ERROR2BASKET'] = "Ошибка добавления товара в корзину";
$MESS ['SAVE'] = "Прайс будет сохранен в файл";
$MESS ['START'] = "Начать генерацию";
$MESS ['GENERATE'] = "Идет генерация прайс-листа";
$MESS ['FINISH'] = "Генерация прайс-листа окончена";
$MESS ['PAGES'] = "Обработано страниц";
$MESS ['FROM'] = "из";
$MESS ['NAME'] = "Наименование";
$MESS ['ID'] = "Код";
$MESS ['TITLE'] = "ПРАЙС-ЛИСТ на";
$MESS ['ERROR_TYPE'] = "Ошибка. Тип файла не выбран";
?>
