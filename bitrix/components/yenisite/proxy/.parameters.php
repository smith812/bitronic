<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
$arComponentParametersMerged['PARAMETERS'] = array(
	"COMPONENT_LIST" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage("COMPONENT_LIST"),
		"TYPE" => "STRING",
		"MULTIPLE" => "Y",
		"REFRESH" => "Y",
		"SIZE" => "4",
	),
);

if(!empty($arCurrentValues["COMPONENT_LIST"]))
{
	$componentFolder = BX_ROOT . '/components';
	foreach($arCurrentValues["COMPONENT_LIST"] as $componentName)
	{
		$path = $componentFolder.CComponentEngine::makeComponentPath($componentName);
		
		if(CComponentUtil::isComponent($path))
		{
			Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . $path . '/.parameters.php');
			include $_SERVER['DOCUMENT_ROOT'] . $path . '/.parameters.php';
			
			foreach($arComponentParameters['PARAMETERS'] as $oldKey => $arParam)
			{
				$newKey = strtoupper(str_replace(array('.',':'), '_', $componentName).'-'.$oldKey);
				
				$arParam['NAME'] = $arParam['NAME']."({$componentName})";
				$arComponentParameters['PARAMETERS'][$newKey] = $arParam;
				unset($arComponentParameters['PARAMETERS'][$oldKey]);
			}
			
			$arComponentParametersMerged = array_merge_recursive($arComponentParametersMerged, $arComponentParameters);
		}
	}
}

$arComponentParameters = $arComponentParametersMerged;
