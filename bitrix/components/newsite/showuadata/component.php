<? 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
    die();
}

if(\NewSite\UniversalAnalytics\UserIdManager::isSearchEngine()) 	// �� �������� ��� ������������ � �������, ��� ������� � API ������ � ��� �����
{
	return false;
}

$userId = CNewsiteUAMain::GetUserId(true);
CNewsiteUAMain::setCookiesAndSessionVars($userId);
$arResult["UA_UID"] = $arParams["BEFORE_USERID"].$userId.$arParams["AFTER_USERID"];

$this->IncludeComponentTemplate();
?>