<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
	die();	
}  
$arComponentDescription = array(
	"NAME" => GetMessage("NEWSITE_SHOWUSERID_MODULE_NAME"),
	"DESCRIPTION" => GetMessage("NEWSITE_SHOWUSERID_MODULE_DESCRIPTION"),
	"PATH" => array(
		"ID" => "newsite.showuadata"
	)
);
?>