<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Список дилеров",
    "DESCRIPTION" => "Список дилеров",
    "ICON" => "/images/strings.gif",
    "PATH" => array(
        "ID" => "newsite",
        "NAME" => "Newsite",
        "CHILD" => array(
            "ID" => "branches.map.list",
            "NAME" => "Список дилеров"
        )
    ),
);
?>