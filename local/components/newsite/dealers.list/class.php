<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/lib/exception.php");

use Newsite\Base as NB;
use Bitrix\Highloadblock as HL;

class CDealersList extends CBitrixComponent
{

    public $regionPropId = 1312;
    public $brandsPropId = 1319;
    public $brandsHlbId = 2;
    public $brandsHlb;

    public function onPrepareComponentParams($arParams)
    {
        if(!\Bitrix\Main\Loader::includeModule("iblock"))
            throw new \Exception(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        if(!\Bitrix\Main\Loader::includeModule("newsite.base"))
            throw new Exception("Модуль newsite.base не установлен");
        if(!\Bitrix\Main\Loader::includeModule("highloadblock"))
            throw new \Exception(GetMessage("HILOADBLOCK_MODULE_NOT_INSTALLED"));

        $result = $arParams;
        $result["CACHE_TIME"] = isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 360000;
        $result["IBLOCK_ID"] = DIALERS_IBLOCK_ID;
        $this->brandsHlb = HL\HighloadBlockTable::getById($this->brandsHlbId)->fetch();

        return $result;
    }

    public function getRegionList(){
        $res = array();

        $dbl = CIBlockPropertyEnum::GetList(array('SORT' => 'ASC', 'VALUE' => 'ASC'), array('PROPERTY_ID' => $this->regionPropId));
        while($arr = $dbl->Fetch()){
            $res[$arr['ID']] = $arr;
        }

        return $res;
    }

    public function getBrandsList(){

        $dbl = CIBlockSection::GetList(array('SORT' => 'ASC', 'NAME' => 'ASC'),
            array('IBLOCK_ID' => CATALOG_IBLOCK_ID, 'ACTIVE' => 'Y', 'SECTION_ID' => false), false,
            array('ID'. 'IBLOCK_ID', 'CODE'));
        $names = array();
        while($arr = $dbl->Fetch()){
            $names[] = strtolower($arr['CODE']);
        }

        $query = new \Bitrix\Main\Entity\Query(HL\HighloadBlockTable::compileEntity($this->brandsHlb));
        $query->setSelect(array('*'));
        $result = $query->exec();

        $result = new CDBResult($result);
        $res = array();
        while ($row = $result->Fetch()){
            if(!in_array(strtolower($row['UF_NAME']), $names)) continue;
            $res[$row['UF_XML_ID']] = $row;
        }
        return $res;

    }

    public function getDealersList(){

        $this->arResult['LIST'] = array();

        $obItems = new NB\ItemsList($this->arParams["IBLOCK_ID"]);
        $obItems->setOrder(array("SORT" => "ASC", "NAME" => "ASC"));
        $this->arResult['LIST'] = $obItems->getList();

    }


    public function executeComponent()
    {

        define('YANDEX_MAP', 'Y');
        //if($this->startResultCache())
        {
            $this->getDealersList();
            $this->arResult["REGIONS"] = $this->getRegionList();
            $this->arResult["BRANDS"] = $this->getBrandsList();

            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }


}