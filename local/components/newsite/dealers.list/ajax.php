<?

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Newsite\Base as NB;

CBitrixComponent::includeComponentClass("newsite:dealers.list");

$params = array();

try{
    $list = new CDealersList();
    $list->arParams = $list->onPrepareComponentParams($params);
    $list->getDealersList();
}catch (Exception $e){
    echo $e->getMessage();
}

