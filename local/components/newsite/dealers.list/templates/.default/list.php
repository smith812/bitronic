<div class="dealers-table">
    <table>
        <thead>
            <tr>
                <td>Название<br>УНП</td>
                <td>Регион</td>
                <td>Адрес</td>
                <td>Контакты</td>
                <td>Бренды</td>
                <td>Описание</td>
            </tr>
        </thead>
        <tbody>
            <?foreach($arResult['LIST'] as $item):
                $contacts = '';
                if(!empty($item['PHONES']['TEXT'])) $contacts .= $item['PHONES']['TEXT'] . ' <br>';
                if(!empty($item['SITE'])) $contacts .= $item['SITE'] . ' <br>';
                if(!empty($item['EMAIL'])) $contacts .= $item['EMAIL'];

                $description = '';
                if(!empty($item['SERVICE_SENTER'])) $description .= '<li>Свой сервис-центр</li>';
                if(!empty($item['ONLINE_STORE'])) $description .= '<li>Интернет-магазин</li>';
                if(!empty($item['SHOWROOM'])) $description .= '<li>Выставочный зал</li>';
                $coord = explode(',', $item['COORD']);
                ?>
                <tr class="catalog-item catalog-table-item wow fadeIn"
                    data-id="<?=$item['ID']?>"
                    data-region="<?=$item['REGION_XML_ID']?>"
                    data-service_senter="<?= empty($item['SERVICE_SENTER']) ? '0' : '1' ?>"
                    data-online_store="<?= empty($item['ONLINE_SRORE']) ? '0' : '1' ?>"
                    data-showroom="<?= empty($item['SHOWROOM']) ? '0' : '1' ?>"
                    data-lat="<?=$coord[0]?>"
                    data-long="<?=$coord[1]?>"
                    >
                    <td><span class="name"><?=$item['NAME']?></span><br><?=$item['UNP']?></td>
                    <td><?=$item['REGION']?><br><span class="city"><?=$item['CITY']?></span></td>
                    <td class="address"><?=$item['ADDRESS']['TEXT']?></td>
                    <td class="contacts"><?=$contacts?></td>
                    <td class="brands">
                        <? if(!empty($item['BRANDS'])):?>
                            <ul>
                                <?foreach($item['BRANDS'] as $brand):?>
                                    <?if(!empty($arResult['BRANDS'][$brand]['UF_NAME'])):?>
                                        <li data-id="<?=$brand?>"><?=$arResult['BRANDS'][$brand]['UF_NAME'] ?></li>
                                    <?endif?>
                                <?endforeach?>
                            </ul>
                        <? endif;?>
                    </td>
                    <td>
                        <?if(!empty($description)):?>
                            <ul><?=$description?></ul>
                        <?endif?>
                    </td>
                </tr>
            <?endforeach;?>
        </tbody>
    </table>
</div>
<div class="dealers-table-small">
    <?foreach($arResult['LIST'] as $item):
        $contacts = '';
        if(!empty($item['PHONES']['TEXT'])) $contacts .= $item['PHONES']['TEXT'] . ' <br>';
        if(!empty($item['SITE'])) $contacts .= $item['SITE'] . ' <br>';
        if(!empty($item['EMAIL'])) $contacts .= $item['EMAIL'];

        $description = '';
        if(!empty($item['SERVICE_SENTER'])) $description .= '<li>Свой сервис-центр</li>';
        if(!empty($item['ONLINE_STORE'])) $description .= '<li>Интернет-магазин</li>';
        if(!empty($item['SHOWROOM'])) $description .= '<li>Выставочный зал</li>';

        $brands = array();
        foreach($item['BRANDS'] as $brand)
            if(!empty($arResult['BRANDS'][$brand]['UF_NAME']))
                $brands[]= $arResult['BRANDS'][$brand]['UF_NAME'];

        ?>
        <div class="item" data-id="<?=$item['ID']?>">
            <div class="caption"><strong><?=$item['NAME']?></strong>, <?=$item['CITY']?>, <?=$item['ADDRESS']['TEXT']?></div>
            <div class="description">
                УНП: <?=$item['UNP']?><br>
                <?=$contacts?><br>
                <?if(!empty($brands)):?>
                    Бренды: <?= implode(', ', $brands)?> <br>
                <?endif?>
                <?if(!empty($description)):?>
                    Сервисы: <ul><?=$description?></ul>
                <?endif?>
            </div>
        </div>
    <?endforeach;?>
</div>
