<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$this->setFrameMode(true);

?>

<div class="dealers-list">
    <div class="dealers-filter">
        <?include "filter.php"?>
    </div>
    <?include 'map.php'?>
</div>
<div class="clear"></div>
<?include "list.php"?>
