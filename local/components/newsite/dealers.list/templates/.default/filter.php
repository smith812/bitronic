<div class="filter">
    <h2>Фильтр</h2>
    <div class="form-wrap">
        <div>
            <label class="textinput-wrapper">
                <span class="text">
                    <span class="inner-wrap">
                        <span class="inner">Область:</span>
                    </span>
                </span>
                <select class="select" name="region">
                    <option value="all" selected>Все</option>
                    <?foreach($arResult['REGIONS'] as $region):?>
                        <option value="<?=$region['XML_ID']?>"><?=$region['VALUE']?></option>
                    <?endforeach?>
                </select>
            </label>
        </div>
        <div>
            <label class="textinput-wrapper">
                <span class="text">
                    <span class="inner-wrap">
                        <span class="inner">Бренд:</span>
                    </span>
                </span>
                <select class="select" name="brand">
                    <option value="all" selected>Все</option>
                    <?foreach($arResult['BRANDS'] as $brand):?>
                        <option value="<?=$brand['UF_XML_ID']?>"><?=$brand['UF_NAME']?></option>
                    <?endforeach?>
                </select>
            </label>
        </div>
        <div>
            <label class="textinput-wrapper">
                <span class="text">
                    <span class="inner-wrap">
                        <span class="inner">Выставочный зал:</span>
                    </span>
                </span>
                <span class="checkbox">
                    <input type="checkbox" name="showroom">
                </span>
            </label>
        </div>
        <div>
            <label class="textinput-wrapper">
                <span class="text">
                    <span class="inner-wrap">
                        <span class="inner">Свой сервис-центр:</span>
                    </span>
                </span>
                <span class="checkbox">
                    <input type="checkbox" name="service_senter">
                </span>
            </label>
        </div>
        <div>
            <label class="textinput-wrapper">
                <span class="text">
                    <span class="inner-wrap">
                        <span class="inner">Интернет-магазин:</span>
                    </span>
                </span>
                <span class="checkbox">
                    <input type="checkbox" name="online_store">
                </span>
            </label>
        </div>
    </div>
</div>
