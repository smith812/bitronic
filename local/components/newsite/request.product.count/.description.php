<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Запрос остатка товаров",
    "DESCRIPTION" => "Запрос остатка товаров",
    "ICON" => "/images/sale_order_full.gif",
    "PATH" => array(
        "ID" => "newsite",
        "NAME" => "Newsite",
        "CHILD" => array(
            "ID" => "request.product.count",
            "NAME" => "Запрос остатка товаров",
        )
    ),
);
?>