<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/lib/exception.php");

class CRequestProductCount extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {
        if(!\Bitrix\Main\Loader::includeModule("iblock"))
            throw new \Exception(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

        if(!\Bitrix\Main\Loader::includeModule("catalog"))
            throw new \Exception(GetMessage("CATALOG_MODULE_NOT_INSTALLED"));

        $result = $arParams;

        if(empty($result['COUNT'])){
            $result['COUNT'] = 1;
        }
        return $result;
    }

    public function getResult(){

        if(empty($this->arParams['ID']))
            throw new \Exception('ID не задан');
        unset($_SESSION['REQUEST_PRODUCT_COUNT'][$this->arParams['ID']]);

        $count = CBasketEvents::GetAvaibleProductCount($this->arParams['ID']);
        $count = CBasketEvents::GetProductCountBySettings($this->arParams['ID'], $count);
        $countOrder = CBasketEvents::GetProductCountBySettings($this->arParams['ID'], $this->arParams['COUNT']);

        $res = array(
            'has_error' => '0',
            'count' => $count,
            'price' => CBasketEvents::GetCurrentBasketPrice($this->arParams['ID'], $this->arParams['COUNT']),
            'count_order' => $countOrder,
            'step' => CBasketEvents::GetProductStepOrder($this->arParams['ID']),
            'min_order' => CBasketEvents::GetProductMinOrder($this->arParams['ID']),
        );
        return $res;
    }

    public function executeComponent()
    {
        if($this->startResultCache())
        {
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }


}