<?

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CBitrixComponent::includeComponentClass("newsite:request.product.count");

$params = array(
    "ID" => $_REQUEST["id"],
    "COUNT" => $_REQUEST['count'],
);

try{
    $obj = new CRequestProductCount();
    $obj->arParams = $obj->onPrepareComponentParams($params);
    $res = $obj->getResult();
}catch (Exception $e){
    $res = array(
        'has_error' => '1',
        'mess' => $e->getMessage(),
    );
}

$APPLICATION->RestartBuffer();

echo json_encode($res);
die();
