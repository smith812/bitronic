<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$this->setFrameMode(true);

?>

<div class="modal fade modal-form modal_productcount" id="modal_productcount" tabindex="-1">
    <div class="modal-dialog">
        <button class="btn-close" data-toggle="modal" data-target="#modal_productcount">
            <span class="btn-text">Закрыть</span>
            <i class="flaticon-close47"></i>
        </button>
        <div class="current-input" data-id="" data-count="" data-source=""></div>
        <div class="content">
            <p>
                Вы хотите заказать <span class="request">5</span> шт.данного товара,
                но в наличии на <span class="date">15:30</span> есть только <span class="count">3</span>.
                Условия поставки недостающего количества <span class="upcount">2</span> Вам сообщит менеджер. Выберите действие:
            </p>
            <div class="textinput-wrapper submit-wrap">
                <button type="button" class="btn-submit btn-main click-buy" data-do="down"><span class="btn-text">Заказать <span class="count"></span> (на складе)</span></button>
            </div>
            <div class="textinput-wrapper submit-wrap">
                <button type="button" class="btn-submit btn-main click-buy" data-do="no"><span class="btn-text">Заказать <span class="request">5</span> шт. (<span class="upcount">2</span>-под заказ</span>)</button>
            </div>
            <div class="request-count">
                <label class="textinput-wrapper">
                    <span class="text"><span class="inner-wrap"><span class="inner">Хотите больше?<br>Введите количество</span></span></span>
                </label>
                <div class="quantity-counter-wrap">
                    <span class="quantity-counter">
                        <button type="button" class="btn-silver quantity-change decrease">
                            <span class="minus">–</span>
                        </button>
                        <input type="text" class="quantity-input textinput" value="1">
                        <button type="button" class="btn-silver quantity-change increase">
                            <span class="plus">+</span>
                        </button>
                    </span>
                </div>
                <div class="btn-buy-wrap text-only">
                    <button type="button" class="btn-submit btn-main click-buy" data-do="count">
                        <span class="text">Заказать</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>