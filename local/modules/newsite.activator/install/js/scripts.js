$(document).ready(function () {

    $(document).on('change', '.newsite_activator select[name=type]', function(){
        var val = $(this).val();
        $('select[name=block] option').hide().attr('data-view', '0');
        $('select[name=block] option[data-type=' + val + ']').show().attr('data-view', '1');
        var block = $('select[name=block] option[data-view="1"]:first').attr('value');
        $('select[name=block] option[value=' + block + ']').attr('selected', true);
        naGetProperties();
    });

    $(document).on('change', '.newsite_activator select[name=block]', function(){
        naGetProperties();
    });

    $(document).on('click', '.newsite_activator .actions button', function(){
        var action = $(this).data('action');
        if(action == 'add-row'){
            naListAddRow();
        }else if(action == 'update'){
            naUpdateSettings();
        }
    })

    $(document).on('change', '.newsite_activator select[name=property]', function(){
        var val = $(this).val(),
            type = $(this).find('option[value=' + val + ']').data('type'),
            container = $(this).closest('tr');
        if(val == '0'){
            container.find('td.value').html('');
            container.find('td.number').html('');
            container.find('td.compare select').attr('name', '');
        }else{
            container.find('td.number').html(val);
            container.find('td.compare select').attr('name', 'compare[' + val + ']');
            if(type == 'L')
            {
                naGetPropEnum(container, val);
            }else{
                container.find('td.value').html('<input type="text" name="value[' + val + ']" >');
            }
        }
    });

    naGetProperties();

})

function naGetProperties()
{
    var query = {
        action: 'properties',
        block: $('.newsite_activator select[name=block]').val()
    }
    $.ajax({
        url: '/ajax/newsite_activator_ajax.php',
        dataType: "html",
        type: "GET",
        data: query,
        success: function(data){
            $('.newsite_activator .items-list').html(data);
        }
    });
}

function naGetPropEnum(container, prop)
{
    var query = {
        action: 'propenum',
        prop: prop
    }
    $.ajax({
        url: '/ajax/newsite_activator_ajax.php',
        dataType: "html",
        type: "GET",
        data: query,
        success: function(data){
            container.find('td.value').html(data);
        }
    });
}

function naUpdateSettings()
{
    $.ajax({
        url: '/ajax/newsite_activator_ajax.php',
        dataType: "html",
        type: "POST",
        data: $('#newsite_activator_form').serialize(),
        success: function(data){
            $('#newsite_activator_form .message').html(data);
            setTimeout(function(){$('#newsite_activator_form .message').html('');}, 5000);
        }
    });
}

function naListAddRow(){
    var row = $('.newsite_activator .items-list tbody tr:first').clone();
    row.find('td.number').html('');
    row.find('td.value').html('');
    $('.newsite_activator .items-list tbody').append(row);
}