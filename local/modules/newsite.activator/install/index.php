<?
IncludeModuleLangFile(__FILE__);

CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;

Class newsite_activator extends CModule
{
    const MODULE_ID = 'newsite.activator';
    var $MODULE_ID = 'newsite.activator';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';
    var $hlBlockTableNames = array(
        "b_activator_settings" => 0,
    );
    var $bots = array('GOOGLE' => 'Google', 'YANDEX' => 'Yandex', 'MAIL' => 'Mail');

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__)."/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("newsite.activator_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("newsite.activator_MODULE_DESC");

        $this->PARTNER_NAME = GetMessage("newsite.activator_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("newsite.activator_PARTNER_URI");
    }

    function InstallDB($arParams = array())
    {
        RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CNewsiteActivator', 'OnBuildGlobalMenu');
        //RegisterModuleDependences("iblock", "OnBeforeIBlockElementUpdate", self::MODULE_ID, "CNewsiteActivator", "checkActiveElement", 1000);
        //RegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", self::MODULE_ID, "CNewsiteActivator", "checkActiveElement", 1000);
        $obUserField = new CUserTypeEntity;

        $data = array(
            'NAME' => "ActivatorSettings",
            'TABLE_NAME' => "b_activator_settings"
        );
        $result = HL\HighloadBlockTable::add($data);
        $ID = $result->getId();
        //поля блока
        $arFields = $this->getArFields($ID, 'IBLOCK_ID', 'integer', "IBLOCK_ID");
        $obUserField->Add($arFields);

        $arFields = $this->getArFields($ID, 'PROPERTY_ID', 'integer', "ID свойства");
        $obUserField->Add($arFields);

        $arFields = $this->getArFields($ID, 'PROPERTY_NAME', 'string', "Свойство");
        $obUserField->Add($arFields);

        $arFields = $this->getArFields($ID, 'COMPARE', 'enumeration', "Сравнение");
        $fId = $obUserField->Add($arFields);

        if(intval($fId) > 0)
        {
            $UserTypeEnum = new CUserFieldEnum();
            $UserTypeEnum->SetEnumValues($fId, array(
                "n0" => array(
                    "SORT" => 100,
                    "XML_ID" => "ANY",
                    "VALUE" => "Любое значение",
                    "DEF" => "Y",
                ),
                "n1" => array(
                    "SORT" => 200,
                    "XML_ID" => "EMPTY",
                    "VALUE" => "Пустое",
                    "DEF" => "N",
                ),
                "n2" => array(
                    "SORT" => 300,
                    "XML_ID" => "EQUAL",
                    "VALUE" => "Равно",
                    "DEF" => "N",
                ),
                "n3" => array(
                    "SORT" => 400,
                    "XML_ID" => "NO_EQUAL",
                    "VALUE" => "Не равно",
                    "DEF" => "N",
                ),
            ));

        }

        $arFields = $this->getArFields($ID, 'VALUE', 'string', "Значение");
        $obUserField->Add($arFields);

    }

    function addAgents(){
        return;
        $date = new DateTime();
        $date->modify('+1 day');
        $strDate = '"' . $date->format('d.m.Y') . '00:00:01' . '"';
        CAgent::AddAgent(
            "CNewsiteActivator::statisticsProcess();",      // имя функции
            "newsite.activator",                            // идентификатор модуля
            "N",                                            // агент не критичен к кол-ву запусков
            86400,                                          // интервал запуска - 1 сутки
            $strDate,                                       // дата первой проверки на запуск
            "Y",                                            // агент активен
            $strDate,                                       // дата первого запуска
            30);
    }

    function removeAgents(){
        return;
        CAgent::RemoveAgent("CNewsiteActivator::statisticsProcess();", "newsite.activator");
    }

    function UnInstallDB($arParams = array())
    {
        UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CNewsiteActivator', 'OnBuildGlobalMenu');
        UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", self::MODULE_ID, "CNewsiteActivator", "checkActiveElement");
        UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementUpdate", self::MODULE_ID, "CNewsiteActivator", "checkActiveElement");

        $rsData = HL\HighloadBlockTable::getList(array("filter" => array("TABLE_NAME"=>array_keys($this->hlBlockTableNames))));

        while($arData = $rsData->Fetch()){
            HL\HighloadBlockTable::delete($arData['ID']);
        }

        return true;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/".self::MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/".self::MODULE_ID."/install/ajax", $_SERVER["DOCUMENT_ROOT"] . "/ajax", true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/".self::MODULE_ID."/install/js",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/" . self::MODULE_ID, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/".self::MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/".self::MODULE_ID."/install/ajax", $_SERVER["DOCUMENT_ROOT"] . "/ajax");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/". self::MODULE_ID."/install/js", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/" . self::MODULE_ID);
        return true;
    }

    function DoInstall()
    {
        global $APPLICATION;
        $this->InstallFiles();
        $this->InstallDB();
        RegisterModule(self::MODULE_ID);
        $this->addAgents();
    }

    function DoUninstall()
    {
        global $APPLICATION;
        $this->removeAgents();
        UnRegisterModule(self::MODULE_ID);
        $this->UnInstallDB();
        $this->UnInstallFiles();
    }

    private function getArFields($id, $name, $type, $label, $default = ''){
        $res = array(
            'ENTITY_ID' => 'HLBLOCK_' . $id,
            'FIELD_NAME' => 'UF_'.$name,
            'USER_TYPE_ID' => $type,
            'XML_ID' => "",
            'SORT' => 100,
            'SHOW_FILTER' => 'N',
            'SETTINGS' => array(
                'DEFAULT_VALUE' => $default,
                'SIZE' => '20',
                'MIN_VALUE' => '0',
                'MAX_VALUE' => '0'
            ),
            'EDIT_FORM_LABEL' => array(
                'ru' => $label,
                'en' => ''
            ),
            'LIST_COLUMN_LABEL' => array(
                'ru' => $label,
                'en' => ''
            ),
            'LIST_FILTER_LABEL' => array(
                'ru' => $label,
                'en' => ''
            ),
            'ERROR_MESSAGE' => array(
                'ru' => '',
                'en' => ''
            ),
            'HELP_MESSAGE' => array(
                'ru' => $label,
                'en' => ''
            ),
        );
        if($type=="boolean"){
            $res['SETTINGS'] = array(
                'DEFAULT_VALUE' => $default,
                'DISPLAY' => 'RADIO',
            );
        }
        return $res;
    }


}
