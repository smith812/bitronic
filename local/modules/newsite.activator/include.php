<?

Bitrix\Main\Loader::includeModule('newsite.activator');


class CNewsiteActivator
{

    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        if($GLOBALS['APPLICATION']->GetGroupRight("main") < "R")
            return;

        $MODULE_ID = str_replace('.', '_', basename(dirname(__FILE__)));
        $aMenu = array(
            "parent_menu" => "global_menu_content",
            "section" => $MODULE_ID,
            "sort" => 360,
            "text" => "Определение активности элемента",
            "title" => 'Определение активности элемента согласно настроек',
            "url" => "/bitrix/admin/".$MODULE_ID."_admin.php",
            "icon" => "",
            "page_icon" => "",
            "items_id" => $MODULE_ID."_items",
            "more_url" => array(),
            "items" => array()
        );

        $aModuleMenu[] = $aMenu;
    }

    static function checkActiveElement(&$arFields){
        //mail('kovalchuk@newsite.by', 'test', print_r($arFields, true));
        $objSettings = new Newsite\Activator\Settings();
        $settings = $objSettings->getIblockSettings($arFields['IBLOCK_ID']);
        if(empty($settings)) return true;

        foreach($settings as $propId => $propSetting){
            if(empty($arFields['PROPERTY_VALUES'][$propId])) continue;
            $active = $objSettings->checkPropertyActive($propSetting, $arFields['PROPERTY_VALUES'][$propId]);
            if(!$active) break;
        }
        $datePropId = $objSettings->getDateCheckPropId($arFields['IBLOCK_ID'], $active);
        if(!empty($datePropId)){
            $arFields['PROPERTY_VALUES'][$datePropId]['n' . count($arFields['PROPERTY_VALUES'][$datePropId])] = array('VALUE' => date('d.m.Y H:i:s'));
        }

        return true;
    }

}
