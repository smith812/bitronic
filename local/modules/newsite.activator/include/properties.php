<?

use Newsite\Activator as NSA;

$block = intval($_REQUEST['block']);
if($block <= 0) die('Некорректный ID инфоблока.');

$objAdmin = new NSA\Admin();
$objSettings = new NSA\Settings();

$settings = $objSettings->getIblockSettings($block);
if(empty($settings)) $settings = array(false => array());
$properties = $objAdmin->getIblockProperties($block);
if(empty($properties)) die("Сойства у инфоблока отсутствуют.");
$compares = $objAdmin->getCompares();
//prent($compares);
?>

<table>
    <thead>
        <tr>
            <td>№ свойства</td>
            <td>Наименование</td>
            <td>Сравнение</td>
            <td>Значение</td>
        </tr>
    </thead>
    <tbody>
        <?foreach($settings as $propSet):
            unset($propType, $selPropType);
            ?>
            <tr>
                <td class="number"><?=$propSet['UF_PROPERTY_ID']?></td>
                <td class="name">
                    <select name="property">
                        <option value="0">Выберите свойство</option>
                        <?foreach($properties as $id => $prop):
                            $propType = $prop['PROPERTY_TYPE'] == 'L' ? 'L' : 'S';
                            ?>
                            <option value="<?=$id?>" data-type="<?=$propType ?>"
                                <?if($id == $propSet['UF_PROPERTY_ID']) {echo 'selected'; $selPropType = $propType;} ?>><?=$prop['NAME']?></option>
                        <?endforeach?>
                    </select>
                </td>
                <td class="compare">
                    <select name="compare[<?=$propSet['UF_PROPERTY_ID']?>]">
                        <?foreach($compares as $id => $item):?>
                            <option value="<?=$id?>" <?if($item['ID'] == $propSet['UF_COMPARE']) echo 'selected' ?> ><?=$item['VALUE']?></option>
                        <?endforeach?>
                    </select>
                </td>
                <td class="value">
                    <?if($selPropType == 'L'):
                        $enumVals = $objAdmin->getPropEnum($propSet['UF_PROPERTY_ID']);
                        ?>
                        <select name="value[<?=$prop?>]">
                            <?foreach($enumVals as $id => $val):?>
                                <option value="<?=$id?>" <?if($id == $propSet['UF_VALUE']) echo ' selected'?>><?=$val['VALUE']?></option>
                            <?endforeach?>
                        </select>
                    <?elseif(!empty($selPropType)):?>
                        <input type="text" name="value[<?=$propSet['UF_PROPERTY_ID']?>]" value="<?=$propSet['UF_VALUE']?>" >
                    <?endif?>
                </td>
            </tr>
        <?endforeach?>
    </tbody>
</table>
