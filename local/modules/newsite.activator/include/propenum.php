<?

use Newsite\Activator as NSA;

$prop = intval($_REQUEST['prop']);
if($prop <= 0) die('Некорректный ID свойства.');

$objAdmin = new NSA\Admin();
$values = $objAdmin->getPropEnum($prop);
if(empty($values)) die('Нет значений.')

?>

<select name="value[<?=$prop?>]">
    <?foreach($values as $id => $val):?>
        <option value="<?=$id?>"><?=$val['VALUE']?></option>
    <?endforeach?>
</select>

