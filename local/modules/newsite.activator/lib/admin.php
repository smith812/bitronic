<?php

namespace Newsite\Activator;

use Bitrix\Highloadblock as HL;

class Admin{

    public $settingsTable = 'b_activator_settings';

    public function __construct(){
        \Bitrix\Main\Loader::includeModule("iblock");
        \Bitrix\Main\Loader::includeModule("catalog");
        \Bitrix\Main\Loader::includeModule("highloadblock");
    }

    public function getIblockTypes(){
        $res = array();
        $dbl = \CIBlockType::GetList(array('SORT' => 'ASC', 'NAME' => 'ASC'), array('ACTIVE' => 'Y'));
        while($arr = $dbl->Fetch()){
            $res[$arr['ID']] = $arr;
        }
        return $res;
    }

    public function getIblocks(){
        $res = array();
        $dbl = \CIBlock::GetList(array(array('SORT' => 'ASC', 'NAME' => 'ASC'), array('ACTIVE' => 'Y')));
        while($arr = $dbl->Fetch()){
            $res[$arr['ID']] = $arr;
        }
        return $res;
    }

    public function getIblockProperties($iblock){
        $res = array();

        $obCatalog = new \CCatalog();
        $arCatalog = $obCatalog->GetByIDExt($iblock);
        if(!empty($arCatalog) && ($arCatalog['CATALOG'] == 'Y')){
            $res = array(
                -1 => array(
                    'PROPERTY_TYPE' => 'S',
                    'NAME' => 'Цена товара',
                ),
                -2 => array(
                    'PROPERTY_TYPE' => 'S',
                    'NAME' => 'Доступное количество товара',
                ),
            );
        }

        $dbl = \CIBlockProperty::GetList(array('NAME' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $iblock));
        while($arr = $dbl->Fetch()){
            $res[$arr['ID']] = $arr;
        }
        return $res;
    }

    public function getCompares(){
        global $USER_FIELD_MANAGER;

        $hlb = HL\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>'b_activator_settings')))->Fetch();
        $fields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlb['ID']);
        $UserTypeEnum = new \CUserFieldEnum();
        $dbl = $UserTypeEnum->GetList(array(), array('USER_FIELD_ID' => $fields['UF_COMPARE']['ID']));
        $res = array();
        while($arr = $dbl->Fetch()){
            $res[$arr['XML_ID']] = $arr;
        }
        return $res;
    }

    public function getPropEnum($id){
        $res = array();
        $dbl = \CIBlockPropertyEnum::GetList(array('SORT' => 'ASC', 'VALUE' => 'ASC'), array('PROPERTY_ID' => $id));
        while($arr = $dbl->Fetch()){
            $res[$arr['XML_ID']] = $arr;
        }
        return $res;
    }

}