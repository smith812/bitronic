<?php

namespace Newsite\Activator;

use Bitrix\Highloadblock as HL;

class Settings{

    private $hlb;
    private $entity;
    private $compares;

    public function __construct(){
        \Bitrix\Main\Loader::includeModule("iblock");
        \Bitrix\Main\Loader::includeModule("highloadblock");
        $this->hlb = HL\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>'b_activator_settings')))->Fetch();
        $this->entity = HL\HighloadBlockTable::compileEntity($this->hlb);
    }

    public function getIblockSettings($iblock){
        $res = array();
        $query = new \Bitrix\Main\Entity\Query($this->entity);
        $query->setSelect(array('*'));
        $query->setFilter(array('UF_IBLOCK_ID' => $iblock));

        $result = $query->exec();
        $dbl = new \CDBResult($result);
        while($arr = $dbl->Fetch()){
            $res[$arr['UF_PROPERTY_ID']] = $arr;
        }
        return $res;
    }

    private function deleteIblockSettings($iblock){
        $setting = $this->getIblockSettings($iblock);
        if(empty($setting)) return;

        $dataClass = $this->entity->getDataClass();
        foreach($setting as $item){
            $dataClass::delete($item['ID']);
        }
    }

    private function addDatePropsToIblock($iblock){
        $props = array(
            'NA_DATE_ACTIVATED' => array(
                'NAME' => 'Дата активации модулем newsite.activator'
            ),
            'NA_DATE_DEACTIVATED' => array(
                'NAME' => 'Дата деактивации модулем newsite.activator'
            ),
        );

        $res = array();
        $ibp = new \CIBlockProperty;
        foreach($props as $prop => $vals){

            if($arProp = \CIBlockProperty::GetByID($prop, $iblock)->Fetch()) {
                $res[] = 'Свойство "' . $prop . '" уже имеется.';
                continue;
            }

            $arFields = Array(
                'NAME' => $vals['NAME'],
                'ACTIVE' => 'Y',
                'SORT' => '10000',
                'CODE' => $prop,
                'MULTIPLE' => 'Y',
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'DateTime',
                'IS_REQUIRED' => 'N',
                'IBLOCK_ID' => $iblock
            );
            $id = $ibp->Add($arFields);
            if(!$id){
                $res[] = $ibp->LAST_ERROR;
            }
        }
        return $res;
    }

    public function getDateCheckPropId($iblock, $active){
        $propCode = $active ? 'NA_DATE_ACTIVATED' : 'NA_DATE_DEACTIVATED';
        $arr = \CIBlockProperty::GetByID($propCode, $iblock)->Fetch();
        return $arr['ID'];

    }

    public function addDateCheckActive($iblock, $id, $active){
        $propCode = $active ? 'NA_DATE_ACTIVATED' : 'NA_DATE_DEACTIVATED';
        $dbl = \CIBlockElement::GetProperty($iblock, $id, array(), array('CODE' => $propCode));
        $value = array();
        while($arr = $dbl->Fetch()){
            $value[] = $arr['VALUE'];
        }
        $value[] = date('d.m.Y H:i:s');
        \CIBlockElement::SetPropertyValuesEx($id, $iblock, array($propCode => $value));
    }

    public function save($data){

        $iblock = intval(trim(htmlspecialchars($data['block'])));
        if($iblock <= 0) return 'Некорректный IBLOCK_ID';

        $dataClass = $this->entity->getDataClass();
        $objAdmin = new Admin();
        $properties = $objAdmin->getIblockProperties($iblock);
        if(empty($properties)) return 'Отсутствуют свойства';
        $compares = $objAdmin->getCompares();

        $this->deleteIblockSettings($iblock);
        $cnt = 0;
        $errors = array();
        $values = array();
        foreach($data['value'] as $propId => $value){
            $values[$propId] = trim(htmlspecialchars($data['value'][$propId]));
        }
        foreach($data['compare'] as $propId => $compare){

            if(($propId > 0) && empty($properties[$propId])) {
                $errors[] = $propId . ' нет в списке';
                continue;
            }

            if(in_array($compare, array('EQUAL', 'NO_EQUAL')) && empty($values[$propId])) {
                $errors[] = $propId . ' отсутствует значение "' . $values[$propId] . '".';
                continue;
            }

            $data = array(
                'UF_IBLOCK_ID' => $iblock,
                'UF_PROPERTY_ID' => $propId,
                'UF_PROPERTY_NAME' => $properties[$propId]['NAME'],
                'UF_COMPARE' => $compares[$compare]['ID'],
                'UF_VALUE' => $values[$propId],
            );

            $result = $dataClass::add($data);
            if($result->getId()){
                $cnt++;
                $errors[] = $propId . ' внесена';
            }else{
                $errors[] = implode(', ', $result->getErrors());
            }
        }
        if($cnt > 0){
            $resAdd = $this->addDatePropsToIblock($iblock);
            $errors = array_merge($errors, $resAdd);
        }
        return "Внесено $cnt записей <br>" . implode('<br>', $errors);
    }

    public function checkPropertyActive($propSetting, $propValue){
        if(empty($this->compares)){
            $objAdmin = new Admin();
            $compares = $objAdmin->getCompares();
            foreach($compares as $item){
                $this->compares[$item['ID']] = $item['XML_ID'];
            }
        }
        foreach($propValue as $arr){
            $value = trim(htmlspecialchars($arr['VALUE']));
            if($this->compares[$propSetting['UF_COMPARE']] == 'ANY'){
                if(empty($value)) return false;
            }elseif($this->compares[$propSetting['UF_COMPARE']] == 'EMPTY'){
                if(!empty($value)) return false;
            }elseif($this->compares[$propSetting['UF_COMPARE']] == 'EQUAL'){
                if($value != $propSetting['UF_VALUE']) return false;
            }elseif($this->compares[$propSetting['UF_COMPARE']] == 'NO_EQUAL'){
                if($value == $propSetting['UF_VALUE']) return false;
            }
        }
        return true;
    }
}