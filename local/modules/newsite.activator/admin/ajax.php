<?

use Newsite\Activator as NSA;

define("ADMIN_MODULE_NAME", "newsite.activator");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if (!$USER->IsAdmin())
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule(ADMIN_MODULE_NAME))
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$baseDir = $_SERVER['DOCUMENT_ROOT'] . "/local/modules/newsite.activator/include/";
$actions = array('properties', 'propenum', 'savesettings');

if(in_array($_REQUEST['action'], $actions)){
    require $baseDir . $_REQUEST['action'] . '.php';
}else{
    die('Некорректный запрос');
}


