<?

use Newsite\Activator as NSA;

define("ADMIN_MODULE_NAME", "newsite.activator");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if (!$USER->IsAdmin())
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule(ADMIN_MODULE_NAME))
{
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$APPLICATION->SetTitle("Настройки определения активности элемента");

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

/*
$APPLICATION->AddHeadString('
	<script type="text/javascript" src="/bitrix/js/newsite.activator/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="/bitrix/js/newsite.activator/scripts.js"></script>
	<link rel="stylesheet" type="text/css" href="/bitrix/js/newsite.activator/styles.css" />
');
*/
$APPLICATION->AddHeadString('
	<script type="text/javascript" src="/bitrix/js/newsite.activator/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="/local/modules/newsite.activator/install/js/scripts.js"></script>
	<link rel="stylesheet" type="text/css" href="/local/modules/newsite.activator/install/js/styles.css" />
');

$objAdmin = new NSA\Admin();
$iblockTypes = $objAdmin->getIblockTypes();
$iblocks = $objAdmin->getIblocks();
?>

    <div class="newsite_activator">
        <form action="#" id="newsite_activator_form" method="post">
            <input type="hidden" name="action" value="savesettings">
            <div class="item-row">
                <div>
                    <label>Тип информационного блока:</label>
                    <select name="type">
                        <? $bFirst = true;
                        foreach($iblockTypes as $id => $item):
                            if($bFirst){
                                $currType = $id;
                                $bFirst = false;
                            }
                            ?>
                            <option value="<?=$id?>"><?=$item['NAME']?></option>
                        <?endforeach?>
                    </select>
                    &nbsp;&nbsp;
                    <br><br>
                </div>
                <div>
                    <label>Информационный блок:</label>
                    <select name="block">
                        <?foreach($iblocks as $id => $item):?>
                            <option value="<?=$id?>" data-type="<?=$item['IBLOCK_TYPE_ID']?>"
                                <?if($item['IBLOCK_TYPE_ID'] != $currType):?> style="display: none;" <?endif?> ><?=$item['NAME']?></option>
                        <?endforeach?>
                    </select>
                    &nbsp;&nbsp;
                    <br><br>
                </div>
                <div class="button-wrap actions">
                    <button type="button" data-action="update">Записать</button>
                </div>
                <div class="clear"></div>
                <div class="message" style="color: red;"></div>
            </div>
            <div class="items-list"></div>
            <div class="actions">
                <button type="button" data-action="add-row">Добавить</button>
            </div>
        </form>
    </div>

<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
