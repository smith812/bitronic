<?
namespace Newsite\Base;

class Element{
    protected $iblockID = 0;
    protected $elementID = 0;
    protected $thereIs = false;
    protected $filter = array();
    protected $order = array();
    protected $fields = array();
    protected $properties = array();

    function __construct($iblockID, $elementID=0){
        if(!\Bitrix\Main\Loader::includeModule("iblock"))
            throw new \Exception(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

        $this->iblockID = $iblockID;
        $this->setElementID($elementID);
    }

    public function setElementID($id){
        if(empty($id)) return false;
        $filter = array(
            array(
                "LOGIC" => "OR",
                array("ID" => $id),
                array("CODE" => $id),
            )
        );
        $this->setFilter($filter);
        $this->order = array();
        return $this->getElement();
    }

    public function getFirstInBlock($order = array("SORT"=>"ASC")){
        $this->order = $order;
        $this->setFilter(array());
        return $this->getElement();
    }

    protected function getElement(){
        $obEl = \CIBlockElement::GetList($this->order, $this->filter, false, array("nTopCount"=>1))->GetNextElement();
        if($obEl){
            $fields = $obEl->GetFields();
            $this->properties = $obEl->GetProperties();
            $this->fields = array();
            foreach($fields as $code=>$value)
                if((strpos($code, "~")===false)&&!empty($value)) $this->fields[$code] = $value;
            foreach($this->properties as $code=>$value)
                if((strpos($code, "~")===false)&&!empty($value)) {
                    if($value["WITH_DESCRIPTION"]=="Y"){
                        if(is_array($value["VALUE"])){
                            foreach($value["VALUE"] as $k=>$v){
                                if(!empty($v))
                                    $this->fields[$code][] = array("VALUE"=>$v, "DESCRIPTION"=>$value["DESCRIPTION"][$k]);
                            }
                        }else{
                            $this->fields[$code] = array("VALUE"=>$value["VALUE"], "DESCRIPTION"=>$value["DESCRIPTION"]);
                        }
                    }else{
                        $this->fields[$code] = $value["VALUE"];
                    }
                }

            $this->thereIs = true;
            $this->elementID = $this->fields["ID"];
        }else{
            $this->fields = array();
            $this->thereIs = false;
            $this->elementID = 0;
            $this->properties = array();
        }
        return $this->thereIs;
    }

    public function setFilter($filter){
        unset($filter['IBLOCK_ID'], $filter["ACTIVE"]);
        $this->filter = array_merge(array("IBLOCK_ID" => $this->iblockID, "ACTIVE" => "Y"), $filter);
    }

    public function hasElement(){
        return $this->thereIs;
    }

    public function getFields($names = array()){
        if(empty($names)||!is_array($names)){
            return $this->fields;
        }else{
            $res = array();
            foreach($names as $name){
                if(!empty($this->fields[$name])) $res[$name] = $this->fields[$name];
            }
            return $res;
        }
    }

    public function getField($name){
        if(!empty($name)&&!empty($this->fields[$name])) return $this->fields[$name];
        return false;
    }
}