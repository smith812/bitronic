<?
namespace Newsite\Base;

class Response
{
	private static $_instance = null;
	private $result = '';
	private $message = '';
	private $values = null;
	
	private function __construct()
	{
	}

	static public function getInstance()
	{
		if (is_null(self::$_instance))
			self::$_instance = new self();
		return self::$_instance;
	}

	public static function setResponse($is_success, $message)
	{
		$obj = Response::getInstance();
		$obj->setResult($is_success, $message);
		return $obj;
	}

	private function setResult($value, $message = '')
	{
		$this->result = $value ? 'success' : 'error';
		$this->message = $message;
		return $this;
	}

	public function setValues(array $arr)
	{
		$this->values = $arr;
		return $this;
	}

	public function send()
	{
		$obj = Response::getInstance();

		global $APPLICATION;
		$APPLICATION->RestartBuffer();

		header("content-type: application/json");
		echo json_encode(array(
			'response' => array('result' => $obj->result, 'message' => $obj->message),
			'values' => $obj->values
		));
	}


}