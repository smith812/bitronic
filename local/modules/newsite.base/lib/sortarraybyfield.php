<?
namespace Newsite\Base;

/**
 * Class SortArrayByField
 * сортирует двумерный ассоциативный массив по заданному полю
 * @package Newsite\Base
 */
class SortArrayByField
{
    private $fieldName = "";

    private function sortAsc($a, $b)
    {
        if ($a[$this->fieldName] == $b[$this->fieldName]) {
            return 0;
        }
        return ($a[$this->fieldName] < $b[$this->fieldName]) ? -1 : 1;
    }

    private function sortDesc($a, $b)
    {
        if ($a[$this->fieldName] == $b[$this->fieldName]) {
            return 0;
        }
        return ($a[$this->fieldName] > $b[$this->fieldName]) ? -1 : 1;
    }

    public function Exec($arr, $fieldName, $asc = true)
    {
        if(empty($fieldName))
            new \Exception('Field name empty');
        $this->fieldName = $fieldName;

        if($asc){
            uasort($arr, array($this, 'sortAsc'));
        }else{
            uasort($arr, array($this, 'sortDesc'));
        }
        return $arr;
    }

}