<?
namespace Newsite\Base;

class ItemsList
{
    protected $iblockID = 0;
    protected $filter = array();
    protected $order = array();
    protected $navParams = false;
    protected $propNames = array();
    protected $fieldNames = array();
    protected $elHelper;
    public $recordCount = 0;

    function __construct($iblockID, $filter = array(), $order = array())
    {
        if(!\Bitrix\Main\Loader::includeModule("iblock"))
            throw new \Exception(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

        $this->iblockID = $iblockID;
        $this->elHelper = new ElementHelper($iblockID);
        $this->setFilter($filter);
        $this->setOrder($order);
    }

    public function setFilter($filter = array()){
        unset($filter['IBLOCK_ID'], $filter["ACTIVE"]);
        $this->filter = array_merge(array("IBLOCK_ID" => $this->iblockID, "ACTIVE" => "Y"), $filter);
    }

    public function setOrder($order = array()){
        $this->order = $order;
    }

    public function getList($names = array(), $numPage = 0, $pageSize = 0){
        $this->setElementFieldNames($names);
        $this->setNavParams($numPage, $pageSize);
        $list = array();
        $dbl = \CIBlockElement::GetList($this->order, $this->filter, false, $this->navParams);
        $this->recordCount = $dbl->SelectedRowsCount();
        while($obEl = $dbl->GetNextElement()){
            $item = array();
            $fields = $obEl->GetFields();
            $properties = $obEl->GetProperties();

            foreach($fields as $code=>$value)
                if((strpos($code, "~")===false)&&!empty($value)) {
                    $item[$code] = $value;
                }
            /*if($fields['ID'] == 7833){
                prent($properties);
                die();
            }*/
            foreach($properties as $code=>$value)
            {
                if((strpos($code, "~")===false)&&!empty($value))
                {
                    if ( is_array($value['VALUE']) && is_array($value['PROPERTY_VALUE_ID']) )
                    {
                        if($value['WITH_DESCRIPTION'] == "Y"){
                            foreach ( $value['VALUE'] as $key=>$val ) {
                                $item[$code][$value['PROPERTY_VALUE_ID'][$key]]['VALUE'] = $val;
                                $item[$code][$value['PROPERTY_VALUE_ID'][$key]]['DESCRIPTION'] = $value['DESCRIPTION'][$key];
                            }
                        }else{
                            foreach ( $value['VALUE'] as $key=>$val ) {
                                $item[$code][$value['PROPERTY_VALUE_ID'][$key]] = $val;
                            }
                        }
                    }elseif(!empty($value['DESCRIPTION'])){
                        $item[$code] = array(
                            "VALUE" => $value["VALUE"],
                            "DESCRIPTION" => $value["DESCRIPTION"]
                        );
                    }else{
                        $item[$code] = $value["VALUE"];
                        if(($value['PROPERTY_TYPE'] == 'L') && ($value['LIST_TYPE'] == 'L')){
                            $item[$code . '_XML_ID'] = $value['VALUE_XML_ID'];
                        }
                    }
                }
            }
            foreach($item as $code=>$value){
                if(strpos($code, "PICTURE")!==false){
                    if(is_array($value)){
                        $item[$code] = array();
                        foreach($value as $pic){
                            $item[$code][] = \CFile::GetFileArray($pic);
                        }
                    }else{
                        $item[$code] = \CFile::GetFileArray($value);
                    }
                }
            }
            if(empty($names)||in_array("EDIT_LINK", $names))
                $item = array_merge($item, $this->elHelper->getEditButtons($item["ID"]));

            $list[$item["ID"]] = $item;
        }
        if(empty($names)){
            return $list;
        }else{
            $res = array();
            foreach($list as $id=>$item){
                $vals = array();
                foreach($names as $name){
                    if(!empty($item[$name])) $vals[$name] = $item[$name];
                }
                $res[$id] = $vals;
            }
            return $res;
        }
    }

    public function getIdsList(){
        $ids = array();
        $dbl = \CIBlockElement::GetList($this->order, $this->filter, false, false, array("ID"));
        while($arId = $dbl->Fetch()){
            $ids[] = $arId["ID"];
        }
        return $ids;
    }

    protected function setNavParams($numPage = 0, $pageSize = 0){
        $numPage = intval($numPage);
        $pageSize = intval($pageSize);

        if($numPage && $pageSize){
            $this->navParams['iNumPage'] = $numPage;
            $this->navParams['nPageSize'] = $pageSize;
        }else $this->navParams = false;
    }

    protected function setElementFieldNames($names = array()){
        global $nsIBElementFields;
        if(empty($names)||!is_array($names)){
            $this->propNames = array();
            $this->fieldNames = array();
            return;
        }
        foreach($names as $name)
            if(in_array($name, $nsIBElementFields)){
                $this->fieldNames[] = $name;
            }else{
                $this->propNames[] = $name;
            }
        if(!in_array("ID", $this->fieldNames)) $this->fieldNames[] = "ID";
        if(!in_array("IBLOCK_ID", $this->fieldNames)) $this->fieldNames[] = "IBLOCK_ID";
    }
}
