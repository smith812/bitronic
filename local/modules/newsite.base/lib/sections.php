<?
namespace Newsite\Base;

class Sections
{
    protected $iblockID = 0;
    protected $filter = array();
    protected $order = array();

    public function __construct($iblockID)
    {
        if (!\Bitrix\Main\Loader::includeModule("iblock"))
            throw new \Exception(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

        $this->iblockID = $iblockID;
        $this->setFilter(array());
    }


    public function setFilter($filter = array()){
        unset($filter['IBLOCK_ID'], $filter["ACTIVE"]);
        $this->filter = array_merge(array("IBLOCK_ID" => $this->iblockID, "ACTIVE" => "Y"), $filter);
    }

    public function setOrder($order = array()){
        $this->order = $order;
    }

    public function getList(){
        $dbl = \CIBlockSection::GetList($this->order, $this->filter, false, array("*", "UF_*"));
        $res = array();
        while($arRes = $dbl->Fetch()){
            $res[$arRes['ID']] = $arRes;
        }
        return $res;
    }


    /*
     * строит дерево разделов.
     * передаётся список ID разделов, которые должны присутствовать в дереве
     */
    public function getSectionsTreeByIds($ids = array(), $level, $urlTemplate, $selectedId = 0){
        if (empty($ids))
            throw new \Exception("Array IDS sections is empty.");

        $filter = array(
            "<=DEPTH_LEVEL" => $level,
            "ACTIVE"        => "Y",
            "ID"            => $ids,
            "IBLOCK_ID"     => $this->iblockID
        );

        $dbl = \CIBlockSection::GetList(
            array("LEFT_MARGIN" => "ASC", "SORT" => "ASC"), $filter, false,
            array("ID", "NAME", "CODE", "IBLOCK_SECTION_ID", "SORT", "DEPTH_LEVEL")
        );

        $sections = array();

        while($item = $dbl->Fetch()){
            $sId = empty($item['IBLOCK_SECTION_ID']) ? 0 : $item['IBLOCK_SECTION_ID'];
            $item['IS_SELECTED'] = $item['ID'] == $selectedId;
            $item['URL'] = str_replace(array('#SECTION_ID#', '#SECTION_CODE#'), array($item['ID'], $item['CODE']), $urlTemplate);
            $sections[$sId][$item['ID']] = $item;
        }

        $sort = new SortArrayByField();
        foreach($sections as $key=>$arr){
            $sections[$key] = $sort->Exec($arr, "SORT");
        }

        $this->sections = $sections;

        return $this->getMenu(0);
    }

    /*
     * возвращает массив SECTION_ID => array(ID1, ID2, ..., IDn)
     * $ids - ID элементов, принадлежность к разделам которых надо найти
     */
    public function getSectionsIdToElements($ids){
        if (empty($ids))
            throw new \Exception("Array IDS elements is empty.");

        $list = array();
        foreach($ids as $id){
            $dbl = \CIBlockElement::GetElementGroups($id, false, array("ID", "ACTIVE"));
            while($arSect = $dbl->Fetch()){
                if($arSect["ACTIVE"]=="Y")
                    $list[$arSect["ID"]][] = $id;
            }
        }
        return $list;
    }

    /*
     * рекурсивная функция для построения дерева из массива.
     * в записи должны присутствовать ID записи и PARENT - ID родителя.
     */
    public function buildTree($parent, &$nodes)
    {
        $arr = array();
        foreach($nodes as $v)
            if($v['PARENT']==$parent){
                $v['CHILDREN'] = $this->buildTree($v['ID'], $nodes);
                $arr[] = $v;
            }
        return $arr;
    }

}