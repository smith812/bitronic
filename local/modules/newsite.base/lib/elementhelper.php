<?
namespace Newsite\Base;

class ElementHelper
{
    protected $iblockID = 0;

    function __construct($iblockID){
        if(!\Bitrix\Main\Loader::includeModule("iblock"))
            throw new \Exception(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

        $this->iblockID = $iblockID;
    }

    public function getEditButtons($id){
        $res = array();
        $arButtons = \CIBlock::GetPanelButtons(
            $this->iblockID,
            $id,
            0,
            array("SECTION_BUTTONS"=>false, "SESSID"=>false)
        );
        $res["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
        $res["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
        return $res;
    }


}