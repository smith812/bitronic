<?
/*
 * заготовка класса компонента для работы с отдельным инфоблоком.
 * при обработке параметров проверяются подключение модуля "iblock",
 * наличие в параметрах корректного ID инфоблока, а так же задаётся время и тип кэширования.
 */
namespace Newsite\Base;

class CIblockComponent extends \CBitrixComponent
{
    protected $iblockId = 0;
    public $arResult = array();

    public function onPrepareComponentParams($arParams)
    {
        if(!\Bitrix\Main\Loader::includeModule("iblock"))
            throw new \Exception(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

        $result["CACHE_TYPE"] = !empty($arParams["CACHE_TYPE"]) ? $arParams["CACHE_TYPE"] : "A";
        $result["CACHE_TIME"] = isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000;
        $result["IBLOCK_ID"] = $this->iblockId = $arParams["IBLOCK_ID"];
        if(empty($this->iblockId))
            throw new\Bitrix\Main\ArgumentException("IBLOCK_ID not defined");

        $result['PATH'] = $this->__path;
        return $result;
    }

    public function executeComponent()
    {
        $this->includeComponentTemplate();
        return $this->arResult;
    }


}