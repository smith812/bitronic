<?

/**
 * Вывод перенменной
 * @global type $USER
 * @param type $mas
 * @param type $prent
 * @param type $show
 */
/*function prent($mas, $prent = true, $show = false) {
    global $USER;
    if ($USER->IsAdmin() || $show || defined("SHOW_DEBUG")) {
        echo "<pre style=\"text-align:left; background-color:#CCC;color:#000; font-size:10px; padding-bottom: 10px; border-bottom:1px solid #000;\">";
        if ($prent)
            print_r($mas);
        else
            var_dump($mas);
        echo "</pre>";
    }
}

/**
 * Получаем массив $a[delivery_id] = PaySystemsID (Array)
 * @return bool|CDBResult
 */
function GetSaleDelivery2PaySystem(){

    global $DB;

    $strSql = "SELECT * FROM b_sale_delivery2paysystem ";
    $dbRes = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
    $res = Array();
    while ($arDel2Pay = $dbRes->Fetch()){
        $res[$arDel2Pay['DELIVERY_ID']][] = $arDel2Pay['PAYSYSTEM_ID'];
    }
    return $res;
}

/**
 * первый символ в верхний регистр
 * @param $str
 * @param string $e
 * @return string
 */
function ns_mb_ucfirst($str, $e='utf-8') {
    $fc = mb_strtoupper(mb_substr($str, 0, 1, $e), $e);
    return $fc.mb_substr($str, 1, mb_strlen($str, $e), $e);
}

/**
 * проверяет дату на валидность согласно заданному формату
 * @param $date
 * @param string $format
 * @return bool
 */
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    if(empty($d)){
        //prent( DateTime::getLastErrors());
        return false;
    }
    $dd = $d->format($format);
    return $d && ($dd == $date);
}

function textCut($string, $length = 150)
{
    ++$length;
    if ($length && mb_strlen($string) > $length)
    {
        $str = mb_substr($string, 0, $length);
        $pos = mb_strrpos($str, ' ');
        while(($pos > 0) && in_array(mb_substr($str, $pos - 1, 1), array(".", ",", "!","?"))) $pos--;
        $str = mb_substr($str, 0, $pos);
        return $str . ' …';
    }
    return $string;
}

/**
 * форматирует число к двухзначному виду
 * @param $x
 * @return string
 */
function N0($x){
    return $x < 10 ? "0" . $x : $x;
}

define("CHACHE_IMG_PATH", "{$_SERVER["DOCUMENT_ROOT"]}/images/cache/");
define("RETURN_IMG_PATH", "/images/cache/");

/**
 * Функция масшатибрования изображений с поддержкой кеширования
 * Поддерживает разные режимы работы MODE
 * MODE принимает значения: cut, in, inv, width
 * @param type $params - массив параметров ресайзера
 * @param type $req
 */
function imageResize($params, $filePath) {

    $params["WIDTH"] = !isset($params["WIDTH"]) ? 100 : intval($params["WIDTH"]);
    $params["HEIGHT"] = !isset($params["HEIGHT"]) ? 100 : intval($params["HEIGHT"]);
    $params["MODE"] = !isset($params["MODE"]) ? 'in' : strtolower($params["MODE"]);


    $params["QUALITY"] = (!isset($params["QUALITY"]) || intval($params["QUALITY"]) <= 0 ) ? 100 : $params["QUALITY"];
    $params["HIQUALITY"] = !isset($params["HIQUALITY"]) ? (($params["WIDTH"] <= 200 || $params["HEIGHT"] <= 200) ? 1 : 0 ) : 0;

    $pathToOriginalFile = "{$_SERVER["DOCUMENT_ROOT"]}/{$filePath}";

    if (!file_exists($pathToOriginalFile))
        return;

    $salt = md5(strtolower($filePath) . implode('_', $params));
    $salt = substr($salt, 0, 3) . '/';

    $filename = basename($filePath);
    $pathToFile = $salt . $filename;

    // если изображение существует
    if (is_file(CHACHE_IMG_PATH . $pathToFile) == true) {
        if ($_REQUEST["clear_cache"] == 'IMAGE') { //при очистке кэша
            unlink(RETURN_IMG_PATH . $pathToFile);
        }
        else {
            return RETURN_IMG_PATH . $pathToFile;
        }
    }

    CheckDirPath(CHACHE_IMG_PATH . $salt);


    $imgInfo = getImageSize($pathToOriginalFile);

    if (intval($params["WIDTH"]) == 0)
        $params["WIDTH"] = intval($params["HEIGHT"] / $imgInfo[1] * $imgInfo[0]);

    if (intval($params["HEIGHT"]) == 0)
        $params["HEIGHT"] = intval($params["WIDTH"] / $imgInfo[0] * $imgInfo[1]);


    //если вырезаться будет cut проверка размеров
    if (($params["WIDTH"] > $imgInfo[0] || $params["HEIGHT"] > $imgInfo[1]) && ($params["MODE"] != "in" && $params["MODE"] != "inv")) {
        $params["WIDTH"] = $imgInfo[0];
        $params["HEIGHT"] = $imgInfo[1];
    }

    $im = ImageCreateTrueColor($params["WIDTH"], $params["HEIGHT"]);
    imageAlphaBlending($im, false);
    switch (strtolower($imgInfo["mime"])) {
        case 'image/gif' :
            $i0 = ImageCreateFromGif($pathToOriginalFile);
            $icolor = imagecolorallocate($im, 255, 255, 255);
            imagefill($im, 0, 0, $icolor);
            break;
        case 'image/jpeg' : case 'image/pjpeg' :
        $i0 = ImageCreateFromJpeg($pathToOriginalFile);
        $icolor = imagecolorallocate($im, 255, 255, 255);
        imagefill($im, 0, 0, $icolor);
        break;
        case 'image/png' :
            $i0 = ImageCreateFromPng($pathToOriginalFile);
            $icolor = imagecolorallocate($im, 255, 255, 255);
            imagefill($im, 0, 0, $icolor);
            break;
        default :
            return;
    }

    if (!($imgInfo[0] == $params["WIDTH"] && $imgInfo[1] == $params["HEIGHT"])) {
        switch (strtolower($params["MODE"])) {
            case 'cut' :
                $k_x = $imgInfo [0] / $params["WIDTH"];
                $k_y = $imgInfo [1] / $params["HEIGHT"];
                if ($k_x > $k_y)
                    $k = $k_y;
                else
                    $k = $k_x;
                $pn["WIDTH"] = $imgInfo [0] / $k;
                $pn["HEIGHT"] = $imgInfo [1] / $k;
                $x = ($params["WIDTH"] - $pn["WIDTH"]) / 2;
                $y = ($params["HEIGHT"] - $pn["HEIGHT"]) / 2;


                imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn["WIDTH"], $pn["HEIGHT"], $imgInfo[0], $imgInfo[1]);
                //                ( dst_im,  src_im, dstX, dstY,  srcX,  srcY, int dstW, int dstH, int srcW, int srcH)
                break;

            //вписана в квадрат без маштабирования (картинка может быть увеличена больше своего размера)
            case 'in' :

                if (($imgInfo [0] < $params["WIDTH"]) && ($imgInfo [1] < $params["HEIGHT"])) {
                    $k_x = 1;
                    $k_y = 1;
                }
                else {
                    $k_x = $imgInfo[0] / $params["WIDTH"];
                    $k_y = $imgInfo[1] / $params["HEIGHT"];
                }

                if ($k_x < $k_y)
                    $k = $k_y;
                else
                    $k = $k_x;

                $pn["WIDTH"] = intval($imgInfo[0] / $k);
                $pn["HEIGHT"] = intval($imgInfo[1] / $k);

                $x = intval(($params["WIDTH"] - $pn["WIDTH"]) / 2);
                $y = intval(($params["HEIGHT"] - $pn["HEIGHT"]) / 2);

                imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn["WIDTH"], $pn["HEIGHT"], $imgInfo[0], $imgInfo[1]);
                // 1 первый параметр изборажение источник
                // 2 изображение которое вставляется
                // 3 4 -х и у с какой точки будет вставятся в изображении источник
                // 5 6 - ширина и высота куда будет вписано изображение


                break;
            //вписана в квадрат с маштабированием (картинка может быть увеличена)
            case 'inv' :

                $k_x = $imgInfo [0] / $params["WIDTH"];
                $k_y = $imgInfo [1] / $params["HEIGHT"];
                if ($k_x < $k_y)
                    $k = $k_y;
                else
                    $k = $k_x;
                $pn["WIDTH"] = $imgInfo [0] / $k;
                $pn["HEIGHT"] = $imgInfo [1] / $k;
                $x = ($params["WIDTH"] - $pn["WIDTH"]) / 2;
                $y = ($params["HEIGHT"] - $pn["HEIGHT"]) / 2;
                imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn["WIDTH"], $pn["HEIGHT"], $imgInfo[0], $imgInfo[1]);


                if ($params["WIDTH"] == 55 && $params["HEIGHT"] == 45) {
                    imageAlphaBlending($im, true);
                    $waterMark = ImageCreateFromPng($_SERVER["DOCUMENT_ROOT"] . "/img/video.png");
                    imageCopyResampled($im, $waterMark, 0, 0, 0, 0, $params["WIDTH"], $params["HEIGHT"], $params["WIDTH"], $params["HEIGHT"]);
                }

                break;

            case 'width' :
                $factor = $imgInfo[1] / $imgInfo[0]; // определяем пропорцию   height / width

                if ($factor > 1.35) {
                    $pn["WIDTH"] = $params["WIDTH"];
                    $scale_factor = $imgInfo[0] / $pn["WIDTH"]; // коэфффициент масштабирования
                    $pn["HEIGHT"] = ceil($imgInfo[1] / $scale_factor);
                    $x = 0;
                    $y = 0;
                    if (($params["HEIGHT"] / $pn["HEIGHT"]) < 0.6) {
                        //echo 100 / ($pn["HEIGHT"] * 100) / ($params["HEIGHT"] *1.5);
                        $pn["HEIGHT"] = (100 / (($pn["HEIGHT"] * 100) / ($params["HEIGHT"] * 1.3))) * $pn["HEIGHT"];
                        $newKoef = $imgInfo[1] / $pn["HEIGHT"];
                        $pn["WIDTH"] = $imgInfo[0] / $newKoef;

                        $x = ($params["WIDTH"] - $pn["WIDTH"]) / 2;
                        //$y = ($params["HEIGHT"] - $pn["HEIGHT"]) / 2;
                    }

                    imageCopyResampled($im, $i0, $x, $y, 0, 0, $pn["WIDTH"], $pn["HEIGHT"], $imgInfo[0], $imgInfo[1]);
                }
                else {
                    if (($imgInfo [0] < $params["WIDTH"]) && ($imgInfo [1] < $params["HEIGHT"])) {
                        $k_x = 1;
                        $k_y = 1;
                    }
                    else {
                        $k_x = $imgInfo [0] / $params["WIDTH"];
                        $k_y = $imgInfo [1] / $params["HEIGHT"];
                    }

                    if ($k_x < $k_y)
                        $k = $k_y;
                    else
                        $k = $k_x;

                    $pn["WIDTH"] = $imgInfo [0] / $k;
                    $pn["HEIGHT"] = $imgInfo [1] / $k;

                    $x = ($params["WIDTH"] - $pn["WIDTH"]) / 2;
                    $y = ($params["HEIGHT"] - $pn["HEIGHT"]) / 2;
                    imageCopyResampled($im, $i0, $x, $y, 0, 0, $params["MODE"], $pn["HEIGHT"], $imgInfo[0], $imgInfo[1]);
                }
                break;

            default : imageCopyResampled($im, $i0, 0, 0, 0, 0, $params["WIDTH"], $params["HEIGHT"], $imgInfo[0], $imgInfo[1]);
                break;
        }

        if ($params["HIQUALITY"]) {
            $sharpenMatrix = array
            (
                array(-1.2, -1, -1.2),
                array(-1, 20, -1),
                array(-1.2, -1, -1.2)
            );
            // calculate the sharpen divisor
            $divisor = array_sum(array_map('array_sum', $sharpenMatrix));
            $offset = 0;
            // apply the matrix
            imageconvolution($im, $sharpenMatrix, $divisor, $offset);
        }

        //наложение ватермарка
        if (
            isset($params["WATERMARK"]) &&
            strlen($params["WATERMARK"]) &&
            (file_exists($params["WATERMARK"]) || file_exists($_SERVER["DOCUMENT_ROOT"] . $params["WATERMARK"]) ) &&
            $pn["WIDTH"] > 200 &&
            $pn["HEIGHT"] > 200 &&
            strtolower(end(explode(".", $params["WATERMARK"]))) == "png"
        ) {
            imageAlphaBlending($im, true);

            $params["WATERMARK"] = file_exists($params["WATERMARK"]) ? $params["WATERMARK"] : $_SERVER["DOCUMENT_ROOT"] . $params["WATERMARK"];

            $watermarkSize = getImageSize($params["WATERMARK"]);
            $iWatermark = ImageCreateFromPng($params["WATERMARK"]);



            //фиксированный размер ватермарка
            $waterH = $watermarkSize[1];
            $waterW = $watermarkSize[0];

            if ($waterH <= 200 && $waterW <= 200) {
                $waterTop = intval(($pn["HEIGHT"] - $waterH)) - 15;
                $waterLeft = intval(($pn["WIDTH"] - $waterW)) - 20;

                imageCopyResampled($im, $iWatermark, $waterLeft, $waterTop, 0, 0, $waterW, $waterH, $waterW, $waterH);
            }
        }



        switch (strtolower($imgInfo["mime"])) {
            case 'image/gif' :imageSaveAlpha($im, true);
                @imageGif($im, CHACHE_IMG_PATH . $pathToFile);
                break;
            case 'image/jpeg' : case 'image/pjpeg' :@imageJpeg($im, CHACHE_IMG_PATH . $pathToFile, $params["QUALITY"]);
            break;
            case 'image/png' : imageSaveAlpha($im, true);
                @imagePng($im, CHACHE_IMG_PATH . $pathToFile);
                break;
        }
    }
    else {
        copy($pathToOriginalFile, CHACHE_IMG_PATH . $pathToFile);
    }

    return RETURN_IMG_PATH . $pathToFile;
}


